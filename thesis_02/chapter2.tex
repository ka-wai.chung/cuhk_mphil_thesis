\chapter{Gravitational Perturbations of Non-rotating Black Holes}

\section{Introduction}

A final black hole during its ringdown phase is a black hole subjected to gravitational perturbations. 
During the ringdown phase, the event horizon undergoes a series of decaying oscillations. 
The decaying oscillations consist of linear superposition of quasinormal modes.  
Quasinormal modes are the characteristics of the behaviour of the horizon and their frequencies are important observables in gravitational-wave astrophysics. 
To study black-hole quasinormal modes, we need to study the gravitational perturbations of spacetime near a black hole. 
This kind of study, known as black-hole perturbation, is an extension of our study in gravitational waves in the last chapter to the background of curved spacetime. 

This chapter is devoted to reviewing the first method of black-hole perturbation: expanding the perturbed Einstein field equation in the background of curved spacetime. 
After expanding the Einstein field equation near a curved spacetime and picking suitable gauge, we can obtain the master equations which describe the oscillation of the horizon. 
Imposing suitable boundary conditions, black-hole quasinormal modes can be studied by solving the master equations. 
In this chapter, we shall first focus on the cases of non-rotating black hole, i.e. black holes described by metric in the form of 
\begin{equation}\label{eq:schwarzschild black hole}
ds^2 = + \Big( 1 - \frac{2M}{r} \Big) dt^2 - \Big( 1 - \frac{2M}{r} \Big)^{-1} dr^2 - r^2 d \theta^2 - r^2 \sin^2 \theta d \phi^2. 
\end{equation} 
Gravitational perturbations of rotating black holes will be discussed in the coming chapters. 


\section{Odd-Parity Gravitational Perturbations}

To expand the Einstein field equation in the Schwarzschild spacetime, we first write the metric of the perturbed spacetime in the form of 
\begin{equation}
g_{\mu \nu} = g_{\mu \nu}^{(0)} + h_{\mu \nu}, 
\end{equation}
where $ g_{\mu \nu}^{(0)} $ is given by \cref{eq:schwarzschild black hole} for the Schwarzschild spacetime and $ h_{\mu \nu} $ is the metric perturbation.
The contravariant components of the perturbed metric are given by, up to the leading order of $ h^{\mu \nu} $, 
\begin{equation}
g^{\mu \nu} = g^{\mu \nu}_{(0)} - h^{\mu \nu}, 
\end{equation}
where $ g^{\mu \nu}_{(0)} $ is the inverse of $ g_{\mu \nu}^{(0)} $ and 
\begin{equation}
h^{\mu \nu} = g^{\mu \alpha}_{(0)} h_{\alpha \beta} g^{\beta \nu}_{(0)}. 
\end{equation}
The perturbed Christoffel symbols and Ricci tensor are accordingly, 
\begin{equation}
\begin{split}
\Gamma^{\mu}_{\nu \lambda} &= \Gamma^{\mu (0)}_{\nu \lambda} + \delta \Gamma^{\mu}_{\nu \lambda}, \\
\delta \Gamma^{\mu}_{\nu \lambda} &= \frac{1}{2} g^{\mu \rho}_{(0)} \big( \partial_{\nu} h_{\rho \lambda} + \partial_{\lambda} h_{\rho \nu} - \partial_{\rho} h_{\nu \lambda} \big), \\
R_{\mu \nu} &= R_{\mu \nu}^{(0)} + \delta R_{\mu \nu}, \\
\delta R_{\mu \nu} &= \nabla_{\nu} \delta \Gamma^{\rho}_{\mu \rho} - \nabla_{\rho} \delta \Gamma^{\rho}_{\mu \nu}. 
\end{split}
\end{equation}
For vacuum perturbations of black holes ($\delta T_{\mu \nu} = 0$), $h_{\mu \nu}$ satisfies the perturbed Einstein field equation, 
\begin{equation}
\delta R_{\mu \nu} = 0. 
\end{equation} 

To simplify the calculation, we demand the perturbations to satisfy the Regge-Wheeler gauge \cite{Regge_Wheeler} by a gauge transformation. 
Within the Regge-Wheeler gauge, the perturbations are described by two axial ($h_0, h_1$) and four polar functions ($H_0, H_1, H_2, K $), 
\begin{equation}\label{eq:perturbation}
h_{\mu \nu} (t, r, \theta, \phi) = h^{\rm polar}_{\mu \nu} + h^{\rm axial}_{\mu \nu}, 
\end{equation}
where 
\begin{equation}\label{eq:perturbation_axial}
h^{\rm axial}_{\mu \nu} = 
\begin{pmatrix} 
0 & 0 & \frac{h_0}{\sin \theta} \partial_{\phi} Y_{lm}  & - h_0 \partial_{\theta} Y_{lm} \\ 
0 & 0 & \frac{h_1}{\sin \theta} \partial_{\phi} Y_{lm}  & - h_1 \partial_{\theta} Y_{lm} \\ 
\frac{h_0}{\sin \theta} \partial_{\phi} Y_{lm} & \frac{h_1}{\sin \theta} \partial_{\phi} Y_{lm} & 0 & 0\\
- h_0 \partial_{\theta} Y_{lm} & - h_1 \partial_{\theta} Y_{lm} & 0 & 0 \\ 
\end{pmatrix} e^{- i \omega t}, 
\end{equation} 
and the polar (even) sector
\begin{equation}\label{eq:perturbation_polar}
h^{\rm polar}_{\mu \nu} = 
\begin{pmatrix} 
- A(r) H_0 Y_{lm} & - H_1 Y_{lm} & 0 & 0 \\ 
 -H_1 Y_{lm} & - \frac{1}{A(r)} H_2 Y_{lm} & 0 & 0 \\
0 & 0 & -r^2 K Y_{lm} & 0\\
0 & 0 & 0 & - r^2 \sin^2 \theta K Y_{lm} \\ 
\end{pmatrix} e^{- i \omega t}, 
\end{equation} 
where $ Y_{lm} = Y_{lm} (\theta, \phi) $ are spherical harmonic and $ A(r) = 1 - 2M / r $. 

Insert \cref{eq:perturbation} into the Einstein field equation, we have the following system of equations, 
\begin{equation}
\begin{split}
& [4M - l(l+1)r]h_0 + ir(r-2M)\Big(2 \omega h_1 + \omega r \frac{d h_1}{d r} - i r \frac{d^2 h_0}{d r^2} \Big) = 0, \\
& 2ir^2 \omega h_0 + [(r-2M)(2-l-l^2)+r^3 \omega^2]h_i - ir^3 \omega \frac{d h_0}{d r} = 0, \\
& ir^3 \omega h_0 + (r-2M)\Big( 2M h_1 + r(r - 2M) \frac{d h_1}{d r} \Big) = 0. \\
\end{split}
\end{equation}
These equations are not independent. 
$h_0 $ and $ h_1 $ are related through  
\begin{equation}
h_0 = \frac{i}{\omega r^2} \Big( 1 - \frac{2M}{r} \Big)\Big( 2Mh_1 + r (r-2M) \frac{d h_1}{d r} \Big). 
\end{equation}
If we define a new function, 
\begin{equation}
Z^{(-)}(r) = \frac{1}{r} \Big( 1 - \frac{2M}{r} \Big) h_1 (r), 
\end{equation}
such that 
\begin{equation}
h_0 = \frac{i}{\omega} \partial_x \Big( r Z^{(-)} \Big),  
\end{equation}
then the system of equations could be reduced into a single partial differential equation, 
\begin{equation}\label{eq:RG_equation}
\frac{\partial^2 Z^{(-)}}{\partial x^2} + \Big( \omega^2 - V^{(-)}(r) \Big) Z^{(-)} = 0, 
\end{equation}
where $ x $ is the tortoise coordinate defined by
\begin{equation}
\frac{d x}{d r} = \frac{1}{1 - 2M / r}
\end{equation}
and $ V^{(-)}(r) $ is the effective potential, 
\begin{equation}\label{eq:V_RW}
V^{(-)}(r) = \Big( 1 - \frac{2M}{r} \Big) \Big(\frac{l(l+1)}{r^2} - \frac{6M}{r^3} \Big). 
\end{equation}

If we restore $ \omega \rightarrow i \partial / \partial t $, \cref{eq:RG_equation} becomes, 
\begin{equation}
\frac{\partial^2 Z^{(-)}}{\partial x^2} -\frac{\partial^2 Z^{(-)}}{\partial t^2} - V^{(-)}(r) Z^{(-)} = 0,
\end{equation}

\section{Even-Parity Gravitational Perturbations}

The polar-sector perturbation functions, for $ l \geq 0 $, satisfy the following equations, 
\begin{equation}
\begin{split}
&A^2 \frac{d^2 K}{d r^2} + A \Big( 3 - \frac{5M}{5} \Big) \frac{1}{r} \frac{d K}{d r} - \frac{A^2}{r} \frac{d H}{d r} + \frac{A}{r^2} (K-H_2) - l(l+1) \frac{A}{2r^2} (K+H_2) = 0, \\
&\omega \Big(  \frac{d K}{d r} + \frac{1}{r} (K - H_2) - \frac{M}{r^2 A} K\Big) - l(l+1) \frac{H_1}{2r^2} = 0, \\
& \frac{\omega^2}{A^2} K - \frac{1 - M/r}{rA} \frac{d K}{d r} - \frac{2 \omega H_1}{rA} + \frac{1}{r} \frac{d H_0}{d r} - \frac{K - H_2}{r^2A} + \frac{l(l+1)}{2 r^2 A} (K - H_0) = 0, \\
& - \frac{\omega^2}{A} (K+H_2) + A \frac{d^2}{dr^2} (K - H_0) + \Big( 1 - \frac{M}{r} \Big)\frac{2}{r} \frac{d K}{d r} + 2 \omega \frac{d H_1}{dr} + \frac{2 \omega}{rA} \Big(1 - \frac{M}{r} \Big) H_1, \\
& - \frac{1}{r} \Big(1 - \frac{M}{r} \Big) \frac{d H_2}{d r} - \frac{1}{r} \Big(1 + \frac{M}{r} \Big) \frac{d H_0}{d r} - \frac{l(l+1)}{2 r^2} (H_2 - H_0) = 0, \\
\end{split}
\end{equation}
for $ l \geq 1 $, 
\begin{equation}
\begin{split}
& \frac{d (A H_1)}{d r} + i \omega (K+H_2) = 0, \\
& \frac{\omega}{A} H_1 \frac{d}{d r} (K- H_0) = \frac{2M}{A r^2} H_0 - \frac{1 - M / r}{A r} (H_2 - H_0) = 0, \\
\end{split}
\end{equation}
and for $ l \geq 2 $, $ H_2 = H_0 $. 

These equations are not independent. 
In fact, $ K $, $ H_1 $ and $ H_2 $ are related through, 
\begin{equation}
\frac{\lambda r^2 A + Mr - 3M^2 - \omega^2 r^4}{r A} K + \frac{(\lambda+1) M - \omega^2 r^3 }{i \omega r} H_1 - (\lambda r + 3M) H_2 = 0, 
\end{equation}
where $ \lambda = \frac{1}{2} (l-1)(l+2) $. 
Elimination of the system yields, 
\begin{equation}
\begin{split}
& \frac{d K}{d r} + a_1 (r) K + a_2 (r) H_1 = 0, \\
& \frac{d H}{d r} + b_1 (r) K + b_2 (r) H_1 = 0, \\
\end{split}
\end{equation}
where
\begin{equation}
\begin{split}
a_1 (r) & = \frac{2Mr - \lambda Mr - 6 M^2 + \omega^2 r^4}{r(r-2M)(\lambda r+3M)}, \\
a_2 (r) & = \frac{\lambda (\lambda+1)r + 2(\lambda+1)M + \omega^2 r^3}{i \omega r^2 (\lambda r+3M)}, \\
b_1 (r) & = i \omega r \frac{2 \lambda r^2 + 4Mr - 4 \lambda Mr - 9 M^2 - \omega^2 r^4}{(r-2M)^2(\lambda r+3M)}, \\
b_2 (r) & = \frac{3 \lambda M r + m r + 6 M^2 - \omega^2 r^4}{(r-2M)^2(\lambda r+3M)}. \\
\end{split}
\end{equation}
Zerilli found that \cite{Zerilli_01}, if we define 
\begin{equation}
Z^{(+)} = \frac{r^2}{\lambda r + 3M} K + \frac{r A}{i \omega (\lambda r + 3M)} H_1, 
\end{equation}
which follows 
\begin{equation}
\partial_x Z^{(+)} = \frac{- \lambda r^2 + 3 \lambda M r + 3M^2}{A(\lambda r + 3M)^2} K - \frac{\lambda (\lambda+1)r^2 + 3 \lambda M r + 6 M^2}{i \omega r (\lambda r + 3M)^2 }, 
\end{equation}
then the whole system can be reduced to a single equation of $ Z^{(+)} $, 
\begin{equation}
\frac{\partial^2 Z^{(+)}}{\partial x^2} + \Big( \omega^2 - V^{(+)} (r)\Big) Z^{(+)} = 0, 
\end{equation}
where 
\begin{equation} \label{eq:V_Z}
V^{(+)}(r) = \Big( 1 - \frac{2M}{r} \Big) \frac{2 \lambda^2 (\lambda + 1) r^3 + 6 \lambda^2 M r^2 + 18 \lambda M^2 r + 18 M^3}{r^3 (\lambda r + 3M)^2}. 
\end{equation}
In particular, the even-parity perturbation functions, $ H_1 $ and $ K $, can be reverted as 
\begin{equation}
\begin{split}
K & = \frac{\lambda (\lambda+1)r^2 + 3 \lambda M r + 6 M^2}{r^2 (\lambda r + 3M)}Z^{(+)} + A \partial_x Z^{(+)}, \\
H_1 & = - i \omega \frac{\lambda r^2 - 3 \lambda M r - 3 M^2}{r^2 (\lambda r + 3M)}Z^{(+)} - i \omega r \partial_x Z^{(+)}. 
\end{split}
\end{equation}

Similar to the Regge-Wheeler equation, if we restore $ \omega \rightarrow i \partial / \partial t $, the Zerilli equation becomes, 
\begin{equation}
\frac{\partial^2 Z^{(+)}}{\partial x^2} - \frac{\partial^2 Z^{(+)}}{\partial t^2} - V^{(+)} (r) Z^{(+)} = 0, 
\end{equation}


\section{Quasinormal Modes of Black-Hole Ringdown}

\subsection{Boundary Conditions for Black-Hole Quasinormal-Mode}

After laborious calculations, we derive the master equations for the axial and polar gravitational perturbations of non-rotating black holes. 
Both of the master equations can be cast into the form of the Klein-Gordon equation, 
\begin{equation}
\label{eq:Master_equation}
\frac{\partial^2 \Phi}{\partial x^2} - \frac{\partial^2 \Phi}{\partial t^2} - V(r) \Phi = 0,
\end{equation}
where $ \Phi $ is the perturbation function. 

To compute the eigenfrequency for different quasinormal modes, we need to solve the master equations subjected to suitable boundary conditions. 
Since the event horizon of black hole is a one-way boundary: nothing can ever come out of the event horizon. 
Corresponding to this nature, the boundary condition at the event horizon should be purely ingoing, 
\begin{equation}
\Phi (x \rightarrow - \infty) \propto e^{- i \omega x}.
\end{equation}
For detectable black-hole quasinormal modes, the corresponding gravitational waves should be out-going at spatial infinity, 
\begin{equation}
\Phi (x \rightarrow + \infty) \propto e^{+ i \omega x}. 
\end{equation}
More compactly, the boundary conditions can be more compactly written as 
\begin{equation} \label{eq:QNM_BC}
\Phi (|x| \rightarrow + \infty) \propto e^{+ i \omega |x|}. 
\end{equation}

In view of these boundary conditions, \cref{eq:Master_equation} is very different from quantum mechanical problems in at least two aspects. 
First, these boundary conditions do not permit normal-mode solutions. 
Due to the dissipation to the event horizon, the energy of the waves propagating around a black hole is not conserved. 
For this reason, the eigensolutions of the master equations correspond to physical states which are not normalizable.
Thus, these states are known as quasinormal-mode states. 
Second, the operator $\partial^2 + \omega^2 - V $ in \cref{eq:Master_equation} is not Hermitian, because, by the nature of quasinormal modes, the eigenvalues of \cref{eq:Master_equation} are complex. 

\subsection{Solving the Quasinormal-mode Frequencies by Leaver's Method}

Having specified the boundary conditions for the quasinormal-mode solutions, the master equations can be solved. 
We first focus on the Regge-Wheeler equation. 
Following the method by E. W. Leaver \cite{Leaver_01}, we will work in the unit of $ 2M = 1 $ within this section. 
Upon the scaling of $ 2M =1 $, the master equations can be written as 
\begin{equation}
r(r-1) \frac{d^{2} \Phi_{l}}{d r^{2}}+\frac{d \Phi_{l}}{d r}-\left[l(l+1)-\frac{3}{r}-\frac{\omega^{2} r^{3}}{r-1}\right] \Phi_{l}=0, 
\end{equation}
where $ l $ is the azimuthal index (or quantum number) of the solution. 
Note that the magnetic index of quasinormal mode are suppressed in the equation, as axially symmetric perturbations should be dominant for the background of spherical symmetric spacetime. 
$ \Phi_l $ can be expressed in the form of 
\begin{equation}\label{eq:Leaver_sol}
\Phi_l = (r-1)^{-\mathrm{i} \omega} r^{2 \mathrm{i} \omega} e^{\mathrm{i} \omega(r-1)} \sum_{j=0}^{\infty} a_{j}\left(\frac{r-1}{r}\right)^{j}. 
\end{equation}
We then substitute this series into the equation, subjected to the following boundary conditions \cref{eq:QNM_BC}, 
\begin{equation}
\begin{split}
& \Phi_{l}(r \rightarrow +\infty)  \sim e^{\mathrm{i} \omega x} \sim e^{\mathrm{i} \omega(r+\ln r)} \sim r^{\mathrm{i} \omega} e^{\mathrm{i} \omega r}, \\
& \Phi_{l}(r \rightarrow 1) \sim e^{-\mathrm{i} \omega x} \sim e^{-\mathrm{i} \omega \ln (r-1)} \sim(r-1)^{-\mathrm{i} \omega}, 
\end{split}
\end{equation}
then we get a three-term recursion relation for the expansion coefficients, 
\begin{equation}
\begin{array}{l}{\alpha_{0} a_{1}+\beta_{0} a_{0}=0} \\ {\alpha_{n} a_{n+1}+\beta_{n} a_{n}+\gamma_{n} a_{n-1}=0, \quad n=1,2, \dots}\end{array}
\end{equation}
where
\begin{equation}
\begin{array}{l}{\alpha_{n}=n^{2}+(2-2 \mathrm{i} \omega) n+1-2 \mathrm{i} \omega, } \\ {\beta_{n}=-\left[2 n^{2}+(2-8 i \omega) n-8 \omega^{2}-4 \mathrm{i} \omega+l(l+1)+1-4\right], } \\ {\gamma_{n}=n^{2}-4 \mathrm{i} \omega n-4 \omega^{2}-4.}\end{array}
\end{equation}
As we are seeking of converging solutions, \cref{eq:Leaver_sol} converges if the following continued-fraction equation is satisfied, 
\begin{equation}
0=\beta_{0}-\frac{\alpha_{0} \gamma_{1}}{\beta_{1}-} \frac{\alpha_1 \gamma_2}{\beta_2 - } \dots. 
\end{equation}
The $n$-th quasinormal-mode frequency is the most stable solution of 
\begin{equation}
\beta_{n}-\frac{\alpha_{n-1} \gamma_{n}}{\beta_{n-1}-} \frac{\alpha_{n-2} \gamma_{n-1}}{\beta_{n-2}-} \dots \frac{\alpha_{0} \gamma_{1}}{\beta_{0}}=\frac{\alpha_{n} \gamma_{n+1}}{\beta_{n+1}-} \frac{\alpha_{n+1} \gamma_{n+2}}{\beta_{n+2}-} \cdots, 
\end{equation}
where $ n = 1, 2, .... $. 
Using this method, quasinormal-mode frequencies of non-rotating black hole of different masses can be computed. 

\subsection{Solving the Perturbation Function by the Method of Green's Function}

Another method to solve the Klein-Gordon equation (\cref{eq:Master_equation}) is the method of Green's function. 
To get rid of the time dependence, we first transform the problem into the frequency domain.  
As gravitational perturbations during the ringdown phase start at a finite time, right after the final black hole is formed, an adequate transformation to convert $ \Phi $ into the frequency domain is the Laplace transform, 
\begin{equation}
\Psi (x, \omega) = \int_{t_0}^{\infty} d \omega \Phi(x, t) e^{-i \omega t},  
\end{equation}
where $ t_0 $ is the starting time of the ringdown phase. 
Then, \cref{eq:Master_equation} becomes, 
\begin{equation} \label{eq:Master_equation_Laplace_transformed}
\frac{\partial^2\Psi}{\partial x^2} + \Big( \omega^2 - V(r)\Big) \Psi = \mathcal{I}, 
\end{equation}
where $ \mathcal{I} $ is the initial data, 
\begin{equation}
\mathcal{I} = \frac{\partial \Phi}{\partial t}\Big|_{t = t_0} - i \omega \Phi(x, t = t_0). 
\end{equation} 

\cref{eq:Master_equation_Laplace_transformed} is a linear inhomogenous second order differential equation, which can be solved by the method of Green's function \cite{Green_Function_Technique_01, Green_Function_Technique_02, Green_Function_Technique_03, Green_Function_Technique_04}. 
The desired Green's function can be constructed by the two linearly independent solutions of the Regge-Wheeler equation, namely, the down mode, 
\begin{equation}\label{eq:down_mode}
\tilde{u}_{\rm down} (x, \omega) \approx 
\begin{cases}
& e^{- i \omega x}, ~ r \rightarrow r_+ \\
& A_1 (\omega) e^{- i \omega r} + A_2(\omega) e^{i \omega r} , ~ r \rightarrow + \infty \\ 
\end{cases}
\end{equation}
which is purely ingoing at the event horizon and the up mode, 
% When $ \mathcal{I} = 0 $, \ref{eq:SE} admits two linearly independent solutions: the down mode, 
\begin{equation}\label{eq:up_mode}
\tilde{u}_{\rm up} (x, \omega) \approx 
\begin{cases}
& B_1(\omega) e^{-i \omega x} + B_2 (\omega) e^{i \omega x}, ~ r \rightarrow r_+ \\
& e^{i \omega r} , ~ r \rightarrow + \infty \\ 
\end{cases}
\end{equation}
which is purely outgoing at spatial infinity, where $ A_1(\omega) $, $A_2(\omega) $, $ B_1 (\omega) $ and $ B_2 (\omega)$ are functions of $ \omega$. 
If these two solutions are known, the Wronskian of the system can be constructed as 
\begin{equation}
W(\omega) = u_{\rm up} \frac{\partial u_{\rm down}}{\partial x} - u_{\rm down} \frac{\partial u_{\rm up}}{\partial x}. 
\end{equation}
Note that the Wronskian is a function of frequency but not position \cite{Hartle_01}. 
In fact, if we evaluate the Wronskian at $ x \rightarrow \pm \infty $, we have,
\begin{equation}
W = 2 i \omega A_1 (\omega) = 2 i \omega B_2 (\omega). 
\end{equation}
Note that quasinormal-mode solutions correspond to the cases of $ A_1 = B_2 = 0 $. 
Thus, for quasinormal-mode frequencies $ \omega_{\rm QNM}$, we have 
\begin{equation}
W(\omega_{\rm QNM}) = 0. 
\end{equation}

With the Wronskian, the Green's function can then be constructed as 
\begin{equation}
\begin{split}
G(i \omega, x, x^{\prime})=\frac{1}{W(\omega)} [&\theta(x-x^{\prime}) u_{\rm down}(x^{\prime}, \omega) u_{\rm up}(x, \omega)\\ 
&+\theta(x^{\prime}-x) u_{\rm up}(x^{\prime}, \omega) u_{\rm down}(x, \omega) ], 
\end{split}
\end{equation}
where $ \theta $ denotes the step function. 
With the Green's function in our hands, we can readily solve for $ \Psi $, 
\begin{equation}
\Psi(\omega, x)=\int_{-\infty}^{\infty} d x^{\prime} G(\omega, x, x^{\prime}) \mathcal{I} (\omega, x^{\prime}). 
\end{equation}
Explicitly, the above integral could be written as 
\begin{equation}
\begin{split}
\Psi(\omega, x)=& \frac{1}{W(\omega)} u^{\mathrm{up}}(\omega, x) \int_{-\infty}^{x} d x^{\prime} u^{\mathrm{in}}(\omega, x^{\prime}) \mathcal{I}(\omega, x^{\prime}) \\ 
&+\frac{1}{W(\omega)} u^{\mathrm{in}}(\omega, x) \int_{x}^{\infty} d x^{\prime} u^{\mathrm{up}}(\omega, x^{\prime}) \mathcal{I}(\omega, x^{\prime}). 
\end{split}
\end{equation}
In the spatial infinity where we detect gravitational waves, $ x \rightarrow + \infty $, the second term of the above integral vanished, thus \cite{Maggiore_vol_2}, 
\begin{equation}
\begin{split}
\Psi(\omega, x) \approx \frac{1}{W(\omega)} u^{\mathrm{up}}(\omega, x) \int_{- \infty}^{\infty} d x^{\prime} u^{\mathrm{down}}(\omega, x^{\prime}) \mathcal{I}(\omega, x^{\prime}). 
\end{split}
\end{equation}

The time-domain ringdown response of a perturbed black hole can be obtained by performing the inverse Laplace transform of $ \Psi $, 
\begin{equation}\label{eq:IFT}
\Phi(x, t) = \frac{1}{2 \pi} \lim_{\epsilon \rightarrow 0} \int_{-\infty + i \epsilon}^{\infty + i \epsilon} d \omega \Psi(\omega, x) e^{+i \omega t}. 
\end{equation}
When evaluating the inverse Laplace transform, we choose the contour on the complex plane as shown in \cref{fig:contour}.
\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{contour.pdf}
  \caption{Contour used when evaluating inverse Laplace transform. Figure reproduced from \cite{Maggiore_vol_2} with permission. }
  \label{fig:contour}
\end{figure}
By definition, the quasinormal-mode frequencies are zeros of $ W(\omega) $ on the complex plane. 
By the Cauchy integral theorem, the zeros of $ W(\omega) $, which are the poles of the Green's function, contribute to \cref{eq:IFT} as residues. 
Thus, the time-domain waveform of the ringdown phase can be expressed as a linear superposition of quasinormal modes, 
\begin{equation}
h_{\rm RD} (t) = \sum_{nlm} \mathcal{A}_{nlm} e^{-i \tilde{\omega}_{nlm} t},  
\end{equation}
where $ \mathcal{A}_{nlm} $ is the amplitude of the $nlm$-th overtone. 

\subsection{Isospectrality of the Regge-Wheeler and the Zerilli Equation}

So far we have only been discussing the Regge-Wheeler equation. 
At first glance, we might need to solve the Zerilli equation separately for quasinormal-mode frequencies.
To the surprise of many people, these two equations share the same quasinormal-mode spectrum, despite that the two potentials look very different. 
That is, the two equations are isospectral. 
This is because, although the two potentials have different mathematical forms, they are describing almost the same potential. 
\cref{fig:Effective_Potential} plots the values of the Regge-Wheeler (solid lines) and the Zerilli potential (dashed lines) for $ l = 2 $ and $ l=3 $ as functions of $ x $. 
From the plot, the two potentials give almost the same value at a given $ x $. 

\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{Effective_Potential.pdf}
  \caption{The effective potential for the Regge-Wheeler (solid lines) and the Zerilli equation (dashed line) for $ l = 2$ and 3 as functions of $ x $. Despite the expressions of the two potentials are very different, they give almost the same value for a given $ x $ for $ l = 2 $ and 3. Figure modified from \cite{Maggiore_vol_2} with permission. }
  \label{fig:Effective_Potential}
\end{figure}

Later, it was shown that the Regge-Wheeler and the Zerilli equation are related through a supersymmetric (SUSY) transformation. 
If any two operators are related through a SUSY transformation, these two operators give the same spectrum of eigenvalues \cite{SUSY_01, SUSY_02}. 
Consider two operators, $ \mathcal{L}_{1} $ and $ \mathcal{L}_{2}$, which are related through a SUSY transformation, 
\begin{equation}\label{eq:SUSY_relation}
\mathcal{L}_{1} D = D \mathcal{L}_{2} 
\end{equation}
for some non-identically vanishing operator $ D $. 
Suppose that $ \phi $ is an eigenfunction of $ \mathcal{L}_{2} $ of eigenvalue $\lambda$, i.e. $\mathcal{L}_{2} \phi = \lambda \phi$, then 
\begin{equation}
D \mathcal{L}_{2} \phi = \lambda (D \phi) = \mathcal{L}_{1} (D \phi). 
\end{equation}
Hence, $ D \phi $ is an eigenfunction of $ \mathcal{L}_{1} $ of eigenvalue $\lambda$. 
Thus, if two operators are related as in \cref{eq:SUSY_relation}, the two operators are isospectral. 

To show that the Regge-Wheeler and the Zerilli equation are related through a SUSY transformation, we consider the following. 
We first write the Regge-Wheeler equation in a more compact form,
\begin{equation}\label{eq:SUSY_transform}
\mathcal{L}_{\rm RW} \psi = \frac{\partial^2 \psi}{\partial x^2}  + \Big( \omega^2 - V_{\rm RW} \Big) \psi = 0,
\end{equation}
where $ V_{\rm RW} $ is given by \cref{eq:V_RW} and the Zerilli equation as
\begin{equation}
\mathcal{L}_{\rm Z} \psi = \frac{\partial^2 \psi}{\partial x^2}  + \Big( \omega^2 - V_{\rm Z} \Big) \psi = 0,
\end{equation}
where $ V_{\rm Z} $ is given by \cref{eq:V_Z}. 
Then, we we consider an anatz for $ D $ in the form of \cite{Maggiore_vol_2} 
\begin{equation}
D = \frac{d}{d x} - g(x).  
\end{equation}
Then the operator equation for $D$ \cref{eq:SUSY_transform} becomes, 
\begin{equation}
-\frac{d V_{\mathrm{Z}}}{d x}-V_{\mathrm{Z}} \frac{d}{d x}+g V_{\mathrm{Z}}=-2 \frac{d g_{l}}{d x} \frac{d}{d x}-\frac{d^{2} g}{d x^{2}}-V_{\mathrm{RW}} \frac{d}{d x}+V_{\mathrm{RW}} g. 
\end{equation}
Solving the above equation gives, 
\begin{equation}
g = -\frac{r-2M}{r^{2}\left[1+(\lambda r) /\left(3 M \right)\right]} -\frac{\lambda(\lambda+1)}{3M}. 
\end{equation}
The existence of nontrivial $ g (x) $ implies that the Regge-Wheeler equation and the Zerilli equation are isospectral.  

\section{Summary}

In this chapter, we have derived two master equations for gravitational perturbations of non-rotating black holes. 
Each of these equations respectively corresponds to the axial and polar sector of gravitational perturbations.
By solving these equations, quasinormal-mode frequencies of gravitational perturbations of non-rotating black holes can be computed.  
Since the Regge-Wheeler and the Zerilli equation are related through a supersymmetric transformation, they share the same quasinormal-mode spectrum (i.e. isospectral).

Nevertheless, astrophysical black holes, for example, the final black holes resulted from the detected gravitational-wave events, are spinning. 
Thus, there is a gap between the black holes which we have been studied by the master equations and astrophysical black holes. 
This conceptual gap hinders us from fully understanding the black holes we detected. 

To study gravitational perturbations of rotating black holes, one might derive the Regge-Wheeler and the Zerilli equation for rotating black holes by following the similar expansion of the Einstein field equation for the spacetime near rotating black holes.
Nevertheless, due to the complexity of metric of spacetime around a rotating black hole (as we shall see in the next chapter), the expansion has never been carried out \cite{Maggiore_vol_2}. 
Even if someone is willing to pay the endeavour to derive such equations, the resulting equations might not be separable.
In view of these difficulties, alternative formalism to study gravitational perturbations of rotating black holes is needed, which will be introduced in the next chapter.  

