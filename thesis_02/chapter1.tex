\chapter{Introduction}

\textit{Throughout the thesis, unless otherwise specified, $ c = G = 1 $.}

\section{General Relativity as a Theory of Gravity}

The notions of absolute space and absolute time in Newtonian mechanics lead to inconsistencies when considering relativistic motion (at the speed approaching the speed of light). 
One of the inconsistencies is about the speed of light. 
According to Newtonian mechanics, different inertial observers could measure different speeds of light. 
Nevertheless, the speed of light has been measured to be constant regardless of whatever direction of the Earth orbiting around the Sun. 
The inconsistencies are resolved by special relativity by abandoning the notion of absolute spacetime and seeking of covariance of physical laws in inertial frames.
Special relativity is then further generalized to general relativity so as to incorporate gravity into the framework of relativity.
The keys of unification are to abandon the notion of absolute inertial frame and to attribute gravity as geometric effects of spacetime.  
In particular, the whole essence of the second point can be summarized in a short sentence by John Wheeler "spacetime tells matter how to curve; matter tells spacetime how to curve". 

% To describe the curvature of spacetime, we need precise description of spacetime and thereby compute curvature of spacetime. 
In general relativity, spacetime is described as a 4-manifold. 
In a 4-manifold, an event is labeled by a 4-coordinate, say, $ x^{\mu} = (t, x, y, z)$.
Einstein's relativity requires the spacetime interval between any two events to be invariant, i.e. 
\begin{equation}
ds^2 = g_{\mu \nu} dx^{\mu} dx^{\nu},  
\end{equation}
where $ g_{\mu \nu} = \eta_{\mu \nu} = (+1, -1, -1, -1) $ for the Minkowski spacetime, as measured by different inertial observers.
In particular, according to relativity, physics is independent of particular choices of coordinate frame.
Therefore, $ ds^2 $ is the same as measured in Cartesian coordinates, spherical coordinates ($t, r, \theta, \phi$) or cylindrical coordinates ($t, \rho, \phi, z$). 
% With the metric in our hands, length of vector (or co-vector) is defined.

In general relativity, gravity is a manifestation of spacetime curvature. To compute the curvature, we need to compute the Christoffel symbols, which are defined by 
\begin{equation}
\Gamma^{\mu}_{\alpha \beta} = \frac{1}{2} g^{\mu \lambda} \big( \partial_{\alpha} g_{\beta \lambda} + \partial_{\beta} g_{\alpha \lambda} - \partial_{\lambda} g_{\alpha \beta}\big).
\end{equation}  
Having determined the Christoffel symbols, we can then compute the Riemann tensor and Ricci tensor, 
\begin{equation}
\begin{split}
R_{\sigma \mu \nu}^{\rho} &=\partial_{\mu} \Gamma_{\nu \sigma}^{\rho}-\partial_{\nu} \Gamma_{\mu \sigma}^{\rho}+\Gamma_{\mu \lambda}^{\rho} \Gamma_{\nu \sigma}^{\lambda}-\Gamma_{\nu \lambda}^{\rho} \Gamma_{\mu \sigma}^{\lambda}, \\
R_{\alpha \beta} &= R_{\alpha \rho \beta}^{\rho} ,\\  
R &= g^{\alpha \beta} R_{\alpha \beta} .\\
\end{split}
\end{equation} 
 
With these mathematical tools in our hand, we are ready to express the principle of general relativity in the language of Mathematics. 
The central statement of general relativity is the Einstein field equation, which equates energy and momentum to the spacetime curvature,
\begin{equation} \label{eq:EFE}
G_{\mu \nu} = R_{\mu \nu} - \frac{1}{2} g_{\mu \nu} R = 8 \pi T_{\mu \nu}, 
\end{equation} 
where $ G_{\mu \nu} $ is the Einstein tensor and $ T_{\mu \nu} $ is the energy-momentum tensor. 
% An important property of the Einstein tensor is that it obeys the Bianchi identity. 
% Namely, 
% \begin{equation}
% \nabla_{\mu} G^{\mu \nu} = 0. 
% \end{equation} 
% The Bianchi identity is just an manifestation of the conservation of energy and momentum. 

\section{Linear Expansion of the Einstein Field Equation}

Gravitational perturbations of weak-field spacetime propagate as waves \cite{Einstein_01, Einstein_02}. 
In Cartesian coordinates, one can write the metric in the form of 
\begin{equation}\label{eq:metric_expansion}
g_{\mu \nu} = \eta_{\mu \nu} + h_{\mu \nu}, 
\end{equation}
where $ \eta_{\mu \nu} $ is the metric of the Minkowski spacetime, $ h_{\mu \nu} $ is the gravitational perturbations of the Minkowski spacetime. 
In particular, $ |h_{\mu \nu} | << |g_{\mu \nu}| $.
Moreover, for the Minkowski spacetime in Cartesian coordinates, $ \Gamma^{\alpha}_{\mu \nu} [\eta_{\gamma \delta}] = R_{\alpha \beta \mu \nu} [\eta_{\gamma \delta}] = R_{\mu \nu} [\eta_{\gamma \delta}] = R[\eta_{\gamma \delta}] = 0 $. 
The Christoffel symbols, Riemann tensor, Ricci tensor and Ricci scalar up to the leading order of $ h_{\mu \nu} $ are given by 
\begin{equation}
\begin{split}
\Gamma^{\mu}_{\alpha \beta} &= \frac{1}{2} \big( \partial_{\beta} h_{\alpha}^{\mu} + \partial_{\alpha} h_{\beta}^{\mu} - \partial^{\mu} h_{\alpha \beta} \big), \\
R_{\mu \nu \alpha \beta} &= \frac{1}{2} \big( \partial_{\nu \alpha} h_{\mu \beta} + \partial_{\mu \beta} h_{\nu \alpha} - \partial_{\mu \alpha} h_{\nu \beta} - \partial_{\nu \beta} h_{\mu \alpha} \big), \\
R_{\mu \nu} &= \frac{1}{2} \big( \partial_{\nu \alpha} h_{\mu}^{\alpha} + \partial_{\nu \alpha} h_{\mu \alpha} - \partial_{\alpha} \partial^{\alpha} h_{\mu \nu} - \partial_{\mu \nu} h_{\alpha}^{\alpha} \big), \\
R&= \frac{1}{2} \big( \partial^{\alpha \beta} h_{\alpha \beta} - \partial_{\alpha} \partial^{\alpha} h_{\beta}^{\beta} \big),\\
\end{split}
\end{equation} 
where $ h^{\mu}_{\nu} = \eta^{\mu \alpha} h_{\alpha \nu} $.  

Putting all the above quantities into \cref{eq:EFE}, we have the perturbed Einstein field equation, describing gravitational perturbations of the Minkowski spacetime in Cartesian coordinates in the weak-field limit, 
\begin{equation}\label{eq:Perturbed_EFE}
\partial_{\nu} \partial^{\alpha} h_{\mu \alpha} + \partial_{\mu} \partial^{\alpha} h_{\nu \alpha} - \partial_{\alpha} \partial^{\alpha} h_{\mu \nu} - \partial_{\mu \nu} h_{\alpha}^{\alpha} - \eta_{\mu \nu} \big(  \partial_{\alpha \beta} h_{\alpha \beta} - \partial_{\alpha} \partial^{\alpha} h_{\beta}^{\beta} \big) = 16 \pi T_{\mu \nu}. 
\end{equation}

\subsection{The Harmonic Gauge}

\cref{eq:Perturbed_EFE} itself is already a wave equation, except that it is more complicated. 
\cref{eq:Perturbed_EFE} can be simplified if we consider the following transformations. 
We first define $ \bar{h}_{\mu \nu}$ as 
\begin{equation}\label{eq:guage_transformation}
\bar{h}_{\mu \nu} = h_{\mu \nu} - \frac{1}{2} \eta_{\mu \nu} h. 
\end{equation}
Then, we demand $ \bar{h}_{\mu \nu} $ to satisfy the harmonic gauge
\begin{equation}\label{eq:Harmonic_guage}
\partial^{\mu} \bar{h}_{\mu \nu} = 0, 
\end{equation}
by a suitable gauge transformation. 
To seek the desired transformation, we consider the following coordinate (gauge) transformation, 
\begin{equation}
x^{\mu}/ = x^{\mu} + \xi^{\nu}, 
\end{equation}
where $ \xi^{\mu} $ is of the same order of the $ |h_{\mu \nu}| $. 
Upon this gauge transformation, 
\begin{equation}
\overline{h}_{\mu \nu} \rightarrow \tilde{h}_{\mu \nu}^{\prime}=\overline{h}_{\mu \nu}-\left(\partial_{\mu} \xi_{\nu}+\partial_{\nu} \xi_{\mu}-\eta_{\mu \nu} \partial_{\rho} \xi^{\rho}\right), 
\end{equation}
and therefore, 
\begin{equation}
\partial^{\nu} \overline{h}_{\mu \nu} \rightarrow\left(\partial^{\nu} \overline{h}_{\mu \nu}\right)^{\prime}=\partial^{\nu} \overline{h}_{\mu \nu}- \Box \xi_{\mu}, 
\end{equation}
where $\Box = \eta^{\alpha \beta} \partial_{\alpha} \partial_{\beta} $ is the d'Lambertian operator. 
The harmonic gauge condition can be satisfied if we chose, 
\begin{equation}
\Box \xi_{\mu}=\partial^{\nu} \overline{h}_{\mu \nu}. 
\end{equation}
Then \cref{eq:Perturbed_EFE} becomes 
\begin{equation}\label{eq:wave_equation}
\partial_{\alpha} \partial^{\alpha} \bar{h}_{\mu \nu} = -16 \pi T_{\mu \nu}. 
\end{equation}
\cref{eq:wave_equation} is an inhomogeneous equation with source term of energy-momentum tensor, which shows that metric perturbations propagate as waves. 
$ \bar{h}_{\mu \nu} $ is in fact gravitational waves. 
\cref{eq:wave_equation} is the central equation governing the generation and the propagation of gravitational waves in the weak-field limit. 

\subsection{The Transverse-Traceless Gauge}

Specifically, for gravitational waves propagating in vacuum, $ T_{\mu \nu} = 0 $. 
\cref{eq:wave_equation} becomes, 
\begin{equation}\label{eq:WE_TT}
\Box \bar{h}_{\mu \nu} = 0. 
\end{equation}
\cref{eq:WE_TT} is a wave equation of 10 degrees of freedom. 

To simplify the solution of \cref{eq:WE_TT}, we further impose the transverse-traceless (TT) gauge, 
\begin{equation}\label{eq:TT_gauge}
h^{0 \mu}=0, \quad h_{i}^{i}=0, \quad \partial^{j} h_{i j}=0. 
\end{equation}
The coordinate frame satisfying the TT gauge is known as the TT frame. 
By imposing the TT gauge, 8 out of the 10 degrees of freedom are removed. 
The remaining two degrees of freedom correspond to the two polarization modes of gravitational waves, namely, the plus mode and cross mode. 
The polarization tensors of the plus mode ($e_{\mu \nu}^{+}$) and the cross mode ($e_{\mu \nu}^{\times}$) are respective given by 
\begin{equation}
\begin{split}
e_{\mu \nu}^{+} &=\left(\begin{array}{cccc} {0} & {0} & {0} & {0} \\ {0} & {1} & {0} & {0} \\ {0} & {0} & {-1} & {0} \\ {0} & {0} & {0} & {0} \end{array}\right), \\
e_{\mu \nu}^{\times} &=\left(\begin{array}{cccc} {0} & {0} & {0} & {0} \\ {0} & {0} & {1} & {0} \\ {0} & {1} & {0} & {0} \\ {0} & {0} & {0} & {0} \end{array}\right). 
\end{split}
\end{equation}
With these two polarizations, gravitational waves in the TT frame can be decomposed, in general, as 
\begin{equation}
h^{\rm TT}_{\mu \nu} (t) = h_{+} (t)  e_{\mu \nu}^{+} +  h_{\times} (t)  e_{\mu \nu}^{\times}, 
\end{equation} 
where $ h_{+} (t) $ and $ h_{\times} (t) $ are the time-domain gravitational waveform of the plus mode and cross mode respectively. 

Moreover, it is always possible to extract the TT gauge components from a general $ h_{\mu \nu} $ through projection. 
Consider a plane gravitational wave of spatial wave vector $ \mathbf{k} $ propagating in a vacuum as an example.  
We first introduce the following projection tensor for the unit vector $ \hat{\mathbf{n}}=\mathbf{k} /|\mathbf{k}|$,  
\begin{equation}\label{eq:P_operator}
P_{i j}(\hat{\mathbf{n}})=\delta_{i j}-n_{i} n_{j}. 
\end{equation}
Note that $ P_{i k} P_{k j}=P_{i j} $, which is a common property of projection operators.  
Then we further define the Lambda operator, 
\begin{equation}\label{eq:Lambda_operator}
\Lambda_{i j, k l}(\hat{\mathbf{n}})=P_{i k} P_{j l}-\frac{1}{2} P_{i j} P_{k l}. 
\end{equation}
Note that the Lambda operator is still a projection operator, in the sense that $ \Lambda_{i j, k l} \Lambda_{k l, m n}=\Lambda_{i j, m n} $. 
Moreover, the trace of $ \Lambda_{ij, kl} $ over $ ij $ or $ kl $ are zero, 
\begin{equation}\label{eq:Lambda_operator_traceless_prop}
\Lambda_{i i, k l}=\Lambda_{i j, k k}=0. 
\end{equation}
Also, the operator is symmetric under the simultaneous exchange $ (ij) \leftrightarrow (kl)$, 
\begin{equation}
\Lambda_{i j, k l} = \Lambda_{k l, i j}
\end{equation}
By \cref{eq:P_operator} and \cref{eq:Lambda_operator}, we have 
\begin{equation}
\begin{aligned} \Lambda_{i j, k l}(\hat{\mathbf{n}})=& \delta_{i k} \delta_{j l}-\frac{1}{2} \delta_{i j} \delta_{k l}-n_{j} n_{l} \delta_{i k}-n_{i} n_{k} \delta_{j l} \\ &+\frac{1}{2} n_{k} n_{l} \delta_{i j}+\frac{1}{2} n_{i} n_{j} \delta_{k l}+\frac{1}{2} n_{i} n_{j} n_{k} n_{l}.  \end{aligned}
\end{equation}
If we pick 
\begin{equation}
h_{i j}^{\mathrm{TT}}=\Lambda_{i j, k l} h_{k l}. 
\end{equation}
$ h_{kl} $ satifies the TT gauge automatically by the properties of the Lambda operator.  
The above procedures can readily be generalized for the cases of no monochromatic gravitational waves. 

%the above decomposition to the TT gauge can still be applied. 
%We can first decompose the waves into a superposition of plane waves of different frequencies through the Fourier transform. 
% Then projecting the decomposed plane waves onto the TT gauge and performing the inverse Fourier transform, one obtains the desired transformation. 

\subsection{Generation of Gravitational Waves Due to General Sources}

\textit{$c$ and $G$ are restored in this and the coming session. } \\
The gravitational-wave equation with source can readily be solved as 
\begin{equation}\label{eq:h_mu_nu_T_mu_nu}
\overline{h}_{\mu \nu}(t, \mathbf{x})=\frac{4 G}{c^{4}} \int d^{3} x^{\prime} \frac{1}{\left|\mathbf{x}-\mathbf{x}^{\prime}\right|} T_{\mu \nu}\left(t-\frac{\left|\mathbf{x}-\mathbf{x}^{\prime}\right|}{c}, \mathbf{x}^{\prime}\right). 
\end{equation}
$ \overline{h}_{\mu \nu} $ might not satisfy the TT gauge. 
To extract the TT components, we act the Lambda operators on $ \overline{h}_{\mu \nu} $, 
\begin{equation}\label{eq:h_ij_T_mu_nu}
h_{i j}^{\mathrm{TT}}(t, \mathbf{x})=\frac{4 G}{c^{4}} \Lambda_{i j, k l}(\hat{\mathbf{n}}) \int d^{3} x^{\prime} \frac{1}{\left|\mathbf{x}-\mathbf{x}^{\prime}\right|} T_{k l}\left(t-\frac{\left|\mathbf{x}-\mathbf{x}^{\prime}\right|}{c}, \mathbf{x}^{\prime}\right). 
\end{equation}

For gravitational-wave detection, it is more convenient to work in the frequency domain. 
We first perform the Fourier transform to the energy-momentum tensor,  
\begin{equation}\label{eq:T_mu_nu_F_transform}
T_{k l}(t, \mathbf{x})=\int \frac{d^{4} k}{(2 \pi)^{4}} \tilde{T}_{k l}(\omega, \mathbf{k}) e^{-i \omega t+i \mathbf{k} \cdot \mathbf{x}}. 
\end{equation}
If we write $ \mathbf{k} = \omega \mathbf{n} / c$, then \cref{eq:h_ij_T_mu_nu} becomes, 
\begin{equation}\label{eq:h_mu_nu_F_transform}
h_{i j}^{\mathrm{TT}}(t, \mathbf{x})=\frac{1}{r} \frac{4 G}{c^{5}} \Lambda_{i j, k l}(\hat{\mathbf{n}}) \int_{-\infty}^{\infty} \frac{d \omega}{2 \pi} \tilde{T}_{k l}(\omega, \omega \hat{\mathbf{n}} / c) e^{-i \omega(t-r / c)}. 
\end{equation}

Just like the multipole expansion of electromagnetism, \cref{eq:T_mu_nu_F_transform} and \cref{eq:h_mu_nu_F_transform} can be expressed in a power series of $ \omega / c $. 
We first expand, 
\begin{equation}
e^{-i \omega\left(t-r / c+\mathbf{x}^{\prime} \cdot \hat{\mathbf{n}} / c\right)}= e^{-i \omega(t-\tau / c)} \times\left[1-i \frac{\omega}{c} x^{\prime} n^{i}+\frac{1}{2}\left(-i \frac{\omega}{c}\right)^{2} x^{\prime i} x^{\prime j} n^{i} n^{j}+\ldots\right], 
\end{equation}
and similarly 
\begin{equation}
T_{k l}\left(t-\frac{r}{c}+\frac{\mathbf{x}^{\prime} \cdot \hat{\mathbf{n}}}{c}, \mathbf{x}^{\prime}\right) \simeq T_{k l}\left(t-\frac{r}{c}, \mathbf{x}^{\prime}\right) +\frac{x^{\prime} n^{i}}{c} \partial_{0} T_{k l}+\frac{1}{2 c^{2}} x^{\prime i} x^{\prime j} n^{i} n^{j} \partial_{0}^{2} T_{k l}+ \ldots. 
\end{equation}
To save the efforts of writing integrals, we define the following tensors, 
\begin{equation}
\begin{aligned} 
S^{i j}(t) &=\int d^{3} x T^{i j}(t, \mathbf{x}) \\ 
S^{i j, k}(t) &=\int d^{3} x T^{i j}(t, \mathbf{x}) x^{k} \\ 
S^{i j, k l}(t) &=\int d^{3} x T^{i j}(t, \mathbf{x}) x^{k} x^{l}, ... .
\end{aligned}
\end{equation}
Thus \cref{eq:h_ij_T_mu_nu} becomes, 
\begin{equation}
\begin{aligned}
h_{i j}^{\mathrm{TT}}(t, \mathbf{x})= \frac{1}{r} \frac{4 G}{c^{4}} \Lambda_{i j, k l}(\hat{\mathbf{n}}) \times\left[S^{k l}+\frac{1}{c} n_{m} \dot{S}^{k l, m}+\frac{1}{2 c^{2}} n_{m} n_{p} \ddot{S}^{k l, m p}+\ldots\right]_{\mathrm{ret}}, 
\end{aligned}
\end{equation}
where the subscript "ret" means that the mathematical objects inside the bracket are evaluated at retard time. 

We are now in the position to examine the physical meanings of $ S^{i j}, S^{i j, k} $ etc. 
We first expand the $ S^{ij} $ as 
\begin{equation}
S^{ij} = S^{00} + \sum_{i = 1}^{3} S^{0 i} + \sum_{i, j \neq 0} S^{ij}, 
\end{equation}
where 
\begin{equation}
\begin{split}
M = S^{00} &= \frac{1}{c^{2}} \int d^{3} x T^{00}(t, \mathbf{x}), \\
P^{i} = S^{0i} &= \frac{1}{c} \int d^{3} x T^{0 i}(t, \mathbf{x}). \\
\end{split}
\end{equation}
By the conservation of energy and momentum, we have $ \dot{M} = \dot{P}^{i} = 0 $. 

Similarly, if we define, 
\begin{equation}
\begin{aligned} 
M^{i} &=\frac{1}{c^{2}} \int d^{3} x T^{00}(t, \mathbf{x}) x^{i}, \\
M^{i j} &=\frac{1}{c^{2}} \int d^{3} x T^{00}(t, \mathbf{x}) x^{i} x^{j}, 
 \end{aligned}
\end{equation}
and 
\begin{equation}
\begin{split}
\begin{aligned} 
P^{i, j} &=\frac{1}{c} \int d^{3} x T^{0 i}(t, \mathbf{x}) x^{j}, \\ 
P^{i, j k} &=\frac{1}{c} \int d^{3} x T^{0 i}(t, \mathbf{x}) x^{j} x^{k}. 
\end{aligned}
\end{split}
\end{equation}
Thus, $M^{ij} $ and $P^{i, j} $ are the moments of energy and momentum respectively. 
As $ \dot{P}^{i, j}-\dot{P}^{j, i}=S^{i j}-S^{j i}=0 $, we have the following identities, 
\begin{equation}
\begin{aligned} 
\dot{M}^{i} &=P^{i}, \\ 
\dot{M}^{i j} &=P^{i, j}+P^{j, i}, 
\end{aligned}
\end{equation}
and 
\begin{equation}
\begin{aligned} 
\dot{P}^{i, j} &=S^{i j}. 
\end{aligned}
\end{equation}
Therefore, the leading order contribution to gravitational-wave generation in the TT gauge is
\begin{equation}
h_{i j}^{\mathrm{TT}}(t, \mathbf{x})= \frac{1}{r} \frac{4 G}{c^{4}}  \Lambda_{i j, k l} (\hat{\mathbf{n}}) S^{k l} = \frac{1}{r} \frac{4 G}{c^{4}}  \Lambda_{i j, k l} (\hat{\mathbf{n}}) \dot{P}^{kl} = \frac{1}{r} \frac{4 G}{c^{4}}  \Lambda_{i j, k l} (\hat{\mathbf{n}}) \ddot{M}^{kl},  
\end{equation}
which is due to the changes of the mass-quadrupole moment of the system. 

\section{Energy and Momentum of Gravitational Waves}

\subsection{Effective Energy-momentum Tensor of Gravitational Waves}

As in the cases of electromagnetic waves and mechanical waves, gravitational waves carry energy and momentum. 
According to general relativity, energy and momentum are sources of gravity. 
Thus, gravitational waves themselves are also sources of gravity.  

To see how the energy and momentum carried by gravitational waves can curve the background spacetime, we go back to the Einstein field equation.
If we expand the perturbed Ricci tensor beyond the linear order, we have the following terms 
\begin{equation}
R_{\mu \nu}={R}_{\mu \nu}^{(0)}+R_{\mu \nu}^{(1)}+R_{\mu \nu}^{(2)}+\ldots, 
\end{equation}
where $ R^{(0)}_{\mu \nu} $ is the Ricci tensor corresponding to the background metric $ g^{(0)}_{\mu \nu} $, $ R_{\mu \nu}^{(1)} \sim \mathcal{O} (h)$ and $ R_{\mu \nu}^{(2)} \sim \mathcal{O} (h^2) $. 
Explicitly, $ R_{\mu \nu}^{(1)} $ and $ R_{\mu \nu}^{(2)} $ are given by 
\begin{equation}\label{eq:R_higher_order}
\begin{split}
R_{\mu \nu}^{(1)} &=\frac{1}{2}\left({D}^{\alpha} {D}_{\mu} h_{\nu \alpha}+{D}^{\alpha} {D}_{\nu} h_{\mu \alpha}-{D}^{\alpha} {D}_{\alpha} h_{\mu \nu}-{D}_{\nu} {D}_{\mu} h\right), \\ 
R_{\mu \nu}^{(2)}&= \frac{1}{2} {g}^{\rho \sigma} {g}^{\alpha \beta}\left[\frac{1}{2} {D}_{\mu} h_{\rho \alpha} {D}_{\nu} h_{\sigma \beta}+\left({D}_{\rho} h_{\nu \alpha}\right)\left({D}_{\sigma} h_{\mu \beta}-{D}_{\beta} h_{\mu \sigma}\right)\right.\\ 
& \quad \quad \quad \quad \quad +h_{\rho \alpha}\left({D}_{\nu} {D}_{\mu} h_{\sigma \beta}+{D}_{\beta} {D}_{\sigma} h_{\mu \nu}-{D}_{\beta} {D}_{\nu} h_{\mu \sigma}-{D}_{\beta} {D}_{\mu} h_{\nu \sigma}\right) \\ 
& \quad \quad \quad \quad \quad + \left(\frac{1}{2} {D}_{\alpha} h_{\rho \sigma}-    {D}_{\rho} h_{\alpha \sigma}\right)\left({D}_{\nu} h_{\mu \beta}+{D}_{\mu} h_{\nu \beta}-{D}_{\beta} h_{\mu \nu}\right)], 
\end{split}
\end{equation}
where $ D_{\mu} $ are the covariant derivatives defined with respect to the background metric. 
For the cases of vacuum, $ R_{\mu \nu} = 0 $. 
Thus the perturbed Einstein field equation can be written as 
\begin{equation} \label{eq:EFE_expanded}
{R}_{\mu \nu}^{(0)} = - \big( {R}_{\mu \nu}^{(1)} + {R}_{\mu \nu}^{(2)} \big). 
\end{equation}
\cref{eq:EFE_expanded} looks like a field equation with $ {R}_{\mu \nu}^{(1)} + {R}_{\mu \nu}^{(2)} $ being the energy-momentum tensor. 

When expanding the Ricci tensor beyond the linear order, two length scales emerge: first, the wavelength of gravitational waves, $ \lambda $, and, second, the radius of curvature $ L_B$ of the background spacetime, such that $ R \sim \partial^2 h \sim \frac{1}{L_B^2}$.  
In the far field limit from sources, $ L_B >> \lambda $. 
Thus, \cref{eq:EFE_expanded} can be further simplified by considering spatial average within a volume of $ l^3 $ such that $ \lambda << l << L_B $, within which the spatial average of $ R_{\mu \nu}^{(1)} $ is zero, i.e. $ \braket{R_{\mu \nu}^{(1)} } \sim 0 $.
Therefore, effectively, we can define an energy-momentum tensor for \cref{eq:EFE_expanded}, 
\begin{equation}
t_{\mu \nu}=-\frac{c^{4}}{8 \pi G}\left\langle R_{\mu \nu}^{(2)}-\frac{1}{2} {g}_{\mu \nu}^{(0)} R^{(2)}\right\rangle
\end{equation}
such that \cref{eq:EFE_expanded} becomes 
\begin{equation} \label{eq:EFE_expanded_02}
{R}_{\mu \nu}^{(0)} = \frac{8 \pi G}{c^{4}}\left(t_{\mu \nu}-\frac{1}{2} {g}_{\mu \nu}^{(0)} t\right), 
\end{equation}
where $ t = g^{(0)}_{\mu \nu} t^{\mu \nu} $. 

In order to examine the physical meaning of $ t_{\mu \nu} $, we consider the cases of flat spacetime, where $ D_{\mu} \rightarrow \partial_{\mu}$ in \cref{eq:R_higher_order}. 
For $ g_{\mu \nu}^{(0)} = \eta_{\mu \nu}$, $ t_{\mu \nu} $ reduces to 
\begin{equation}
t_{\mu \nu}=\frac{c^{4}}{32 \pi G}\left\langle\partial_{\mu} h_{\alpha \beta} \partial_{\nu} h^{\alpha \beta}\right\rangle. 
\end{equation}
By the harmonic gauge condition, it can be verified that 
\begin{equation} \label{eq:conservation_law}
\partial_{\mu} t^{\mu \nu} = 0. 
\end{equation}
Thus, locally $ t_{\mu \nu} $ can be regarded as an energy-momentum tensor due to gravitational waves. 
In particular, the energy density of gravitational waves is,
\begin{equation}
t^{00}=\frac{c^{2}}{32 \pi G}\left\langle\dot{h}_{i j}^{\mathrm{TT}} \dot{h}_{i j}^{\mathrm{TT}}\right\rangle = \frac{c^{2}}{16 \pi G}\left\langle\dot{h}_{+}^{2}+\dot{h}_{ \times}^{2}\right\rangle. 
\end{equation}
\cref{eq:conservation_law} implies the following identities, 
\begin{equation}
\partial_{0} t^{0 0} + c^{-1} \partial_{i} t^{i 0} = 0. 
\end{equation}
The above equation actually describes the conservation of energy and momentum. 
We recognize the energy carried by gravitational waves within a spatial volume $ V $ is given by 
\begin{equation}
E_{V}=\int_{V} d^{3} x t^{00}. 
\end{equation}
In the content of gravitational-wave detection, what we measure is related to the change of energy carried by gravitational waves $ d E / d t $ and the corresponding change of linear momentum.
By \cref{eq:conservation_law} and Gauss's theorem, one can express $ dE / dt $ in terms of $ h_{ij}^{\rm TT} $, 
\begin{equation} \label{eq:Power_formula}
\frac{d E}{d t}=-\int_{V} d^{3} x \partial_{i} t^{0 i} =-\int_{S} d A n_{i} t^{0 i} = \frac{c^{3} r^{2}}{32 \pi G} \int d \Omega\left\langle\dot{h}_{i j}^{\mathrm{TT}} \dot{h}_{i j}^{\mathrm{TT}}\right\rangle, 
\end{equation}
and for the rate of change of linear momentum, 
\begin{equation}
\frac{1}{c} \frac{d P^k}{d t}= \int_{V} d^{3} x \partial_{0} t^{0 k} = -\int_{S} d A t^{0 k} = -\frac{c^{3}}{32 \pi G} r^{2} \int d \Omega\left\langle\dot{h}_{i j}^{\mathrm{TT}} \partial^{k} h_{i j}^{\mathrm{TT}}\right\rangle. 
\end{equation}

\subsection{Generation and Power of Gravitational Waves generated by Compact Binary Coalescence}

As we have seen from previous calculations, the mass-quadrupole moment is the leading-order multipole which contributes to the generation and the emission power of gravitational waves. 
As an example of the application of the mass-quadrupole radiation, we consider two black holes (or two neutron stars, or a neutron star and a black hole) of masses $ m_1$ and $ m_2 $ in a circular orbit around the common centre of mass (CM) with angular frequency $\omega$. 
During the inspiral phase, the distance $ R $ between the two is much larger than the sizes of black holes. 
Thus the black holes can be viewed as point particles. 
Also, $ d R / d t$ is much smaller than the orbital speed of the black holes. 
Thus, instantaneously, we can regard $ R $ is a constant at every moment. 
Concerning an observer at a distance $r$, the system is oriented such that the line of sight makes an angle $ \theta $ with the normal to the orbital plane.

Without loss of generality, we may assume that the orbital plane lies on the $x - y$ plane. 
In Cartesian coordinates, the displacement between the two black holes is 
\begin{equation}\label{eq:displacement}
\begin{aligned} 
x_{0}(t) &=R \cos \left(\omega_{s} t+ \phi_0 \right), \\ 
y_{0}(t) &=R \sin \left(\omega_{s} t+ \phi_0 \right), \\ 
z_{0}(t) &=0, 
\end{aligned}
\end{equation}
where $ \phi_0 $ is the initial phase. 

The energy-momentum tensor corresponding to this system is 
\begin{equation}
T^{\mu \nu} (\vec{r}, t) = m_1 \frac{d x_1^{\mu}}{d t} \frac{d x_1^{\nu}}{d t} \delta(\vec{r} - \vec{x}_1) + m_2 \frac{d x_2^{\mu}}{d t} \frac{d x_2^{\nu}}{d t} \delta(\vec{r} - \vec{x}_2),  
\end{equation}
where $ x_1^{\mu} $ and $ x_2^{\mu} $ are respectively the 4-displacement of the 1st and 2nd black hole. 
With some algebra, it can be shown that, in terms of the reduced mass $ \mu $
\begin{equation}
\frac{1}{\mu} = \frac{1}{m_1} + \frac{1}{m_2}, 
\end{equation}
and in the coordinate frame where the centre of mass is at the origin, the quadrupole tensor can be written as  
\begin{equation}
M^{i j}=\mu x_{0}^{i}(t) x_{0}^{j}(t). 
\end{equation}
With \cref{eq:displacement}, $M^{i j} $ can be explicitly evaluated as  
\begin{equation}
\begin{aligned}
M_{11} &=\mu R^{2} \frac{1-\cos 2 \omega_{s} t}{2}, \\ 
M_{22} &=\mu R^{2} \frac{1+\cos 2 \omega_{s} t}{2}, \\ 
M_{12} &= -\frac{1}{2} \mu R^{2} \sin 2 \omega_{s} t. 
\end{aligned}
\end{equation}
Taking the second-order time derivative of $ M_{ij} $, we have 
\begin{equation}
\begin{aligned} 
\ddot{M}_{11} &=2 \mu R^{2} \omega_{s}^{2} \cos 2 \omega_{s} t, \\ 
\ddot{M}_{12} &=2 \mu R^{2} \omega_{s}^{2} \sin 2 \omega_{s} t, 
\end{aligned}
\end{equation}
and $ \ddot{M}_{22}=-\ddot{M}_{11} $. 
We have 
\begin{equation}
\begin{aligned} 
h_{+}(t ; \theta, \phi) &=\frac{1}{r} \frac{4 G \mu \omega_{s}^{2} R^{2}}{c^{4}}\left(\frac{1+\cos ^{2} \theta}{2}\right) \cos \left(2 \omega_{s} t_{\mathrm{ret}}+2 \phi\right), \\ 
h_{ \times}(t ; \theta, \phi) &=\frac{1}{r} \frac{4 G \mu \omega_{s}^{2} R^{2}}{c^{4}} \cos \theta \sin \left(2 \omega_{s} t_{\mathrm{ret}}+2 \phi\right). 
\end{aligned}
\end{equation}
As the two black holes are still far away from each other during the inspiral phase, Newtonian mechanics gives a good approximation to the mechanics during this stage. 
Using Kepler's third law to eliminate $ R $ from the equations, we find
\begin{equation}
\begin{aligned}
h_{+}(t) &=\frac{4}{r}\left(\frac{G M_{c}}{c^{2}}\right)^{5 / 3}\left(\frac{\pi f_{\mathrm{gw}}}{c}\right)^{2 / 3} \frac{1+\cos ^{2} \theta}{2} \cos \left(2 \pi f_{\mathrm{gw}} t_{\mathrm{ret}}+2 \phi\right), \\
h_{ \times}(t) &=\frac{4}{r}\left(\frac{G M_{c}}{c^{2}}\right)^{5 / 3}\left(\frac{\pi f_{\mathrm{gw}}}{c}\right)^{2 / 3} \cos \theta \sin \left(2 \pi f_{\mathrm{gw}} t_{\mathrm{ret}}+2 \phi\right),  
\end{aligned}
\end{equation}
where $ \mathcal{M} $,  
\begin{equation}
\mathcal{M}_{c}=\frac{\left(m_{1} m_{2}\right)^{3 / 5}}{\left(m_{1}+m_{2}\right)^{1 / 5}}. 
\end{equation}

By \cref{eq:Power_formula}, the power of gravitational-wave generation by compact binaries can be explicitly evaluated as 
\begin{equation}
\frac{d^2 E}{d t d \Omega } = \frac{2}{\pi} \frac{c^{5}}{G}\left(\frac{G \mathcal{M} \omega}{2 c^{3}}\right)^{10 / 3} \Bigg( \left(\frac{1+\cos ^{2} \theta}{2}\right)^{2}+\cos ^{2} \theta \Bigg). 
\end{equation}

\section{Response of Binary Black-Hole Systems to Emissions of Gravitational Waves}

As gravitational waves carry energy and momentum away from the compact binary, the orbit of the compact binary system shrinks and merges to form a single compact object. 
In general, a binary black-hole coalescence consists of three phases. 
Firstly, the inspiral phase: as the binary system is radiating energy, momentum and angular momentum away through the emission of gravitational waves, the orbit of the binary shrinks. 
According to Kepler's third law, the orbital frequency increases as a result of decreasing orbital radius. 
Therefore, during this stage, the frequency of gravitational waves increases steadily. 
Such a phenomenon is known as "chirp". 
Secondly, the merger phase: when the two black holes of the binary are close enough, they will merge to form a common event horizon. 
During the merger phase, the luminosity of gravitational-wave emission is peaked. 
Finally, the ringdown phase: after the formation of the common event horizon, the new event horizon settles into a stationary event horizon through quasinormal (decaying) oscillations. 
The behaviour of the black hole(s) of these phases can be predicted either by analytical perturbation or numerical simulations. 

\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{GW_IMR.png}
  \caption{The three phases of gravitational-wave generation: firstly, the inspiral phase, when the orbital radius shrinks and frequencies increases as a result of the loss of energy and angular momentum through the transmission of gravitational waves. 
Secondly, the merger phase: when two black holes are close enough, their event horizons merge to form a common one. 
Finally, the ringdown phase, when the event horizon of the newly formed black hole settles down by going through a series of quasinormal oscillations. Figure taken from \url{https://www.soundsofspacetime.org/coalescing-binaries.html}}
  \label{fig:LPT}
\end{figure}


\subsection{Inspiral}

The metric and orbit of compact binary during the inspiral phase can be approximated by an analytical scheme, called the post-Newtonian formalism, a relaxation scheme to approximate solutions of the Einstein field equation. 
We first realize that when two massive objects are far away, their motion can be accurately described by Newtonian mechanics.
To keep track on the order of relaxation, we define a dimensionless parameter $ \epsilon $, 
\begin{equation}
\epsilon = \sqrt{\frac{R_s}{d}}, 
\end{equation}
where $ R_s $ is the Schwarzschild radius (of the larger black hole, or the sum of both black holes) and $ d $ is the coordinate distance between them. 
When $ \epsilon << 1$, the compact binary is orbiting at a low speed $v/c << 1$.  
A natural way to describe the spacetime near a evolving compact binary is to expand the metric, the Einstein field equation and the geodesic equations in power series of $ \epsilon $, 
\begin{equation}
\begin{split}
g_{00} =& -1 +{}^{(2)} g_{00}+{}^{(4)} g_{00} + {}^{(6)} g_{00} ... , \\
g_{0i} =& +{}^{(3)} g_{0i} + {}^{(5)} g_{0i} ...  , \\
g_{ij} =& \delta_{ij} +{}^{(2)} g_{ij} + {}^{(4)} g_{ij} ...  , \\
\end{split}
\end{equation}
where $ {}^{(n)} g_{ij} $ is the $ \mathcal{O} (\epsilon^n) $ term of $ g_{\mu \nu} $. 
Accordingly, we can also expand $ T^{\mu \nu} $ in a similar way, 
\begin{equation}
\begin{split}
T^{00} &=^{(0)} T^{00}+^{(2)} T^{00}+\ldots, \\
T^{0 i} &=^{(1)} T^{0 i}+^{(3)} T^{0 i}+\ldots, \\
T^{i j} &=^{(2)} T^{i j}+^{(4)} T^{i j}+\ldots. 
\end{split}
\end{equation}
If we substitute the above expressions into the Einstein equation, we obtain a system of nonlinear differential equations of different orders of $ \epsilon $. 
After solving the metric upto a given order of $\epsilon$, the corresponding orbit of the binary can be computed by solving the geodesic equations, 
\begin{equation}
\frac{d^{2} x^{i}}{d \tau^{2}}=-\Gamma_{\mu \nu}^{i} \frac{d x^{\mu}}{d \tau} \frac{d x^{\nu}}{d \tau}, 
\end{equation}
where $ \Gamma_{\mu \nu}^{i} $ are the Christoffel symbols computed with the metric obtained up to the desired post-Newtonian order, 
\begin{equation}
\Gamma_{\mu \nu}^{i} = \Gamma_{\mu \nu}^{i}[^{(0)} g_{\mu \nu} + ^{(1)} g_{\mu \nu} + .... ].  
\end{equation} 

% We conclude the session of the post-Newtonian formalism by briefly mentioning the work about higher pN orders. 
In principle, the above-stated procedures can be extended to any higher post-Newtonian order, despite that the computational complexity and intensity will grow significantly with the expansion order.
As of the time of writing the thesis, only the orbit up to 3.5 post-Newtonian order is available \cite{pN_03}. 
For the explicit metric and orbit up to 3.5 post-Newtonian order, the reader can refer \cite{pN_03} and the references therein.  

\subsection{Merger}

The spacetime near the binary during the merger phase is extremely vibrant and dynamic. 
Thus, the spacetime during which can only be studied by numerical relativity. 
Since the 1960s, there have been efforts devoted to simulating the gravitational-wave signatures due to binary black-hole coalescences.
At the beginning of the development, simulations drew heavily on the Arnowitt-Deser-Misner decomposition \cite{ADM_Decomposition}. 
Nevertheless, these efforts did not yield many successes until the first complete and successful simulation of a binary black-hole merger by Frans Pretorius in 2005 \cite{Pretorius_2005}, based on the harmonic gauge decomposition \cite{Pretorius_2005_02} and the subsequent developments of the Baumgarte-Shapiro-Shibata-Nakamura (BSSN) formalism \cite{BSSN_01, BSSN_02}. 
Decades of efforts before the first successful numerical simulation of black-hole mergers manifests the difficulties of the simulations. 

\begin{figure}
     \centering
     \begin{subfigure}
         \centering
         \includegraphics[scale=0.4]{Pretorius_2005_01.png}
         % \caption{$y=x$}
         \label{fig:y equals x}
     \end{subfigure}
     \hfill
     \begin{subfigure}
         \centering
         \includegraphics[scale=0.4]{Pretorius_2005_02.png}
         % \caption{$y=3sinx$}
         \label{fig:three sin x}
     \end{subfigure}
     \caption{(colour lines) The left panel shows the orbit of the equal-mass binary black-hole up to the merger phase of individual mass of $10 M_{\odot}$ with different resolutions, from the first complete and successful simulations of binary black-hole merger. The simulations are done by a numerical relativity code based on generalized harmonic gauge decomposition \cite{Pretorius_2005_02}. The right panel shows the extracted gravitational waves due to the equal masses binary in the same simulation. Figures reproduced from \cite{Pretorius_2005} with permission. }
\end{figure} 

\subsection{Ringdown}

After the merger, the newly formed event horizon will settle into a stationary horizon by going through a series of quasinormal oscillations. 
The modes of the quasinormal oscillations are known as quasinormal modes and are characterized by quasinormal-mode frequencies of the oscillations. 
The quasinormal-mode frequencies of black holes can be studied by black-hole perturbation theory. 
Studies on black-hole perturbation find that quasinormal-mode spectrum of black holes closely depends on the mass and spin of the black hole concerned. 
Quasinormal-mode spectrum of black holes has been exhaustively calculated for different masses and spins.
This spectrum can be used to estimate these two parameters of the final black hole upon ringdown detection. 

Besides serving as a powerful tool to estimate the physical properties of black-hole through gravitational-wave detection, the spectrum also serves as a tool to observe the strong-field dynamics and test general relativity in strong-field regimes, which is the central theme of this thesis. 

