\chapter{Gravitational Perturbations of Rotating Black Holes}

\section{Introduction}

% In the last chapter, we have derived the master equations for gravitational perturbations to spinless black holes, by expanding the perturbed Einstein field equation. 
For a rotating black hole of mass $ M $, angular momentum $ Ma $, its spacetime is described by the Kerr metric \cite{Kerr_01}, 
\begin{equation}\label{eq:Kerr_metric}
\begin{split}
ds^{2}=& \Big(1-\frac{2 M r}{\Sigma} \Big) d t^{2}+\frac{4 M a r \sin ^{2}(\theta)}{\Sigma} dt d\phi-\frac{\Sigma}{\Delta} d r^{2} - \Sigma d \theta^{2} \\
& -\frac{r^{2}+a^{2}+\2 M a^{2} r \sin^{2}\theta}{ \Sigma} \sin^{2}\theta d \phi^{2}, 
\end{split}
\end{equation} 
where $ \Delta = (r - r_+) (r - r_-)$, $ r_+ $ and $ r_- $ being the radius of the outer and the inner event horizon, $ \Sigma = r^2 + a^2 \cos^2 \theta $. 
In contrast to the Schwarzschild solution, \cref{eq:Kerr_metric} posses frame-dragging elements, namely $ g_{t \phi} = \frac{2Ma r \sin^2 \theta}{\Sigma} $. 
The frame-dragging terms make the metric non-diagonal and the computation more lengthy. 
Moreover, it seems to be very difficult to pick for a suitable gauge to simplify the calculation for rotating black holes. 
In view of these difficulties, expanding of the Einstein equation is not a sensible approach to study gravitational perturbations of rotating black holes.  

Fortunately, an alternative formalism of general relativity, known as the spinor-coefficient formalism, or the Newman-Penrose formalism, provides us with the second method of black-hole perturbation. 
This chapter is devoted to reviewing the spinor-coefficient formalism. 
Based on the spinor-coefficient formalism, we derive the Teukolsky equation, a second-order linear partial differential equation describing scalar, vector and tensor perturbations of arbitrarily spinning black holes.
The Teukolsky equation turns out to be a convenient tool to study perturbations of rotating black holes. 
 
\section{Spinor Coefficients}

To define spinor coefficients for the Kerr metric (\cref{eq:Kerr_metric}), the metric is to be decomposed by a set of null tetrad \cite{Penrose_Newman_01}, 
\begin{equation}
g_{\mu \nu} = l_{\mu} n_{\nu} + n_{\mu} l_{\nu} - m_{\mu} \bar{m}_{\nu} - m_{\nu} \bar{m}_{\mu}, 
\end{equation}
where the set of tetrad $ \{ l_{\mu}, n_{\mu}, m_{\mu}, \bar{m}_{\mu} \} $ satisfies the cross-normalization conditions, 
\begin{equation}
\begin{split}
& l_{\mu} l^{\mu} = n_{\mu} n^{\mu} = m_{\mu} m^{\mu} = \bar{m}_{\mu} \bar{m}^{\mu} = 0, \\ 
& l_{\mu} n^{\mu} = m_{\mu} \bar{m}^{\mu} = 1. \\
\end{split}
\end{equation}
For Kerr metric \cref{eq:Kerr_metric}, we chose \cite{Teukolsky_01_PRL, Teukolsky_02_ApJ}
\begin{equation}
\begin{split}
l^{\mu} &=\Big[\frac{r^{2}+a^{2}}{\Delta}, 1,0, \frac{a}{\Delta} \Big] \\
n^{\mu} &=\Big[ \frac{r^{2}+a^{2}}{2 \Sigma},-\frac{\Delta}{2 \Sigma}, 0, \frac{a}{2 \Sigma} \Big] \\
m^{\mu} &= \Big[ \frac{i a \sin \theta}{\sqrt{2} (r+i a \cos \theta)}, 0, \frac{1}{\sqrt{2} (r+i a \cos \theta)}, \frac{i \csc \theta }{\sqrt{2} (r+i a \cos \theta)}\Big]. 
\end{split}
\end{equation}

Having defined these four tetrads, we can further define four directional derivatives, 
\begin{equation}
\begin{split}
D &=\nabla_{\ell}=l^{\mu} \nabla_{\mu}, \Delta =\nabla_{\mathbf{n}}=n^{\mu} \nabla_{\mu}, \delta =\nabla_{\mathbf{m}}=m^{\mu} \nabla_{\mu}, \overline{\delta} =\nabla_{\mathbf{m} }=\overline{\overline{m}}^{\mu} \nabla_{\mu}, 
\end{split}
\end{equation}
12 complex spinor coefficients 
\begin{equation}
\begin{split}
\kappa &=-m^{a} D l_{a}=-m^{a} l^{b} \nabla_{b} l_{a}, \\
\tau &=-m^{a} \Delta l_{a}=-m^{a} n^{b} \nabla_{b} l_{a}, \\ 
\sigma &=-m^{a} \delta l_{a}=-m^{a} m^{b} \nabla_{b} l_{a}, \\
\rho &=-m^{a} \overline{\delta} l_{a}=-m^{a} \overline{m}^{b} \nabla_{b} l_{a}, \\ 
\pi &=\overline{m}^{a} D n_{a}=\overline{m}^{a} l^{b} \nabla_{b} n_{a}, \\
\nu &=\overline{m}^{a} \Delta n_{a}=\overline{m}^{a} n^{b} \nabla_{b} n_{a}, \\ 
\mu &=\overline{m}^{a} \delta n_{a}=\overline{m}^{a} m^{b} \nabla_{b} n_{a}, \\
\lambda &=\overline{m}^{a} \overline{\delta} n_{a}=\overline{m}^{a} \overline{m}^{b} \nabla_{b} n_{a}, \\
\epsilon &=-\frac{1}{2}(n^{a} D l_{a}-\overline{m}^{a} D m_{a})=-\frac{1}{2}(n^{a} l^{b} \nabla_{b} l_{a}-\overline{m}^{a} l^{b} \nabla_{b} m_{a}), \\ 
\gamma &=-\frac{1}{2}(n^{a} \Delta l_{a}-\overline{m}^{a} \Delta m_{a})=-\frac{1}{2}(n^{a} n^{b} \nabla_{b} l_{a}-\overline{m}^{a} n^{b} \nabla_{b} m_{a}), \\
\beta &=-\frac{1}{2}(n^{a} \delta l_{a}-\overline{m}^{a} \delta m_{a})=-\frac{1}{2}(n^{a} m^{b} \nabla_{b} l_{a}-\overline{m}^{a} m^{b} \nabla_{b} m_{a}), \\
\alpha &=-\frac{1}{2}(n^{a} \overline{\delta} l_{a}-\overline{m}^{a} \overline{\delta} m_{a})=-\frac{1}{2}(n^{a} \overline{m}^{b} \nabla_{b} l_{a}-\overline{m}^{a} \overline{m}^{b} \nabla_{b} m_{a}). 
\end{split}
\end{equation}

As the Kerr metric describes a Petrov-type D spacetime, by the Goldberg-Sachs theorem \cite{Goldberg_Sach_01}, the following spinor coefficients vanished, 
\begin{equation}
\begin{split}
&\psi_{0} =\psi_{1} =\psi_{3} =\psi_{4} =0, \\
&\kappa =\sigma =\nu =\lambda =0. \\
\end{split}
\end{equation} 
The other coefficients are 
\begin{equation}
\begin{split}
\rho &=-1 /(r-i a \cos \theta), \\
\beta &=-\rho^{*} \cot \theta /(2 \sqrt{2}), \\
\pi &=i a \rho^{2} \sin \theta / \sqrt{2}, \\ 
\tau &=-i a \rho \rho^{*} \sin \theta / \sqrt{2},\\
\mu &=\rho^{2} \rho^{*} \Delta / 2, \\
\gamma&=\mu+\rho \rho^{*}(r-M) / 2,\\
\alpha&=\pi-\beta^{*}. 
\end{split}
\end{equation}

Gravitational perturbations in the spinor-coefficient formalism are described by the Weyl scalars, 
\begin{equation}
\begin{split}
\psi_{0} &=-C_{\alpha \beta \gamma \delta} l^{\alpha} m^{\beta} l^{\gamma} m^{\delta}, \\
\psi_{1} &=-C_{\alpha \beta \gamma \delta} l^{\alpha} n^{\beta} l^{\gamma} m^{\delta}, \\
\psi_{2} &=-C_{\alpha \beta \gamma \delta} l^{\alpha} m^{\beta} \bar{m}^{\gamma} n^{\delta}, \\
\psi_{3} &=-C_{\alpha \beta \gamma \delta} l^{\alpha} n^{\beta} \bar{m}^{\gamma} m^{\delta}, \\
\psi_{4} &=-C_{\alpha \beta \gamma \delta} n^{\alpha} m^{* \beta} n^{\gamma} m^{* \delta}, \\
\end{split}
\end{equation} 
where, $ \psi_{4} $ corresponds to the out-going part of gravitational perturbation, as in the far-field limit, the fourth Weyl scalar reduces to 
\begin{equation}
\psi_4 \rightarrow  - \frac{1}{8} (\ddot{h}_{+} - i \ddot{h}_{\times} ). 
\end{equation}

There are also 10 independent of Ricci-Newman-Penrose scalars
\begin{equation} 
\begin{aligned}
\Phi_{00} : & :=\frac{1}{2} R_{a b} l^{a} l^{b}, \quad \Phi_{11} :=\frac{1}{4} R_{a b}\left(l^{a} n^{b}+m^{a} \overline{m}^{b}\right), \\ 
\Phi_{01} & :=\frac{1}{2} R_{a b} l^{a} m^{b}, \quad \Phi_{10} :=\frac{1}{2} R_{a b} l^{a} \overline{m}^{b}=\overline{\Phi}_{01}, \\
\Phi_{02} & :=\frac{1}{2} R_{a b} m^{a} m^{b}, \quad \Phi_{20} :=\frac{1}{2} R_{a b} \overline{m}^{a} \overline{m}^{b}=\overline{\Phi}_{02}, \\
\Phi_{12} & :=\frac{1}{2} R_{a b} m^{a} n^{b}, \quad \Phi_{21} :=\frac{1}{2} R_{a b} \overline{m}^{a} n^{b}=\overline{\Phi}_{12}, \\
\Phi_{22} & :=\frac{1}{2} R_{a b} n^{a} n^{b}, \quad \Lambda :=\frac{1}{24} R = \frac{1}{24} g^{\mu \nu} R_{\mu \nu}. 
\end{aligned}
\end{equation}

The energy-momentum tensor on the right-hand side of the Einstein field equation is projected along the chosen tetrad, 
\begin{equation}
\begin{split}
T_{ll} &= T_{\mu \nu} l^{\mu} l^{\nu}, \\
T_{nn} &= T_{\mu \nu} n^{\mu} n^{\nu}, \\
T_{lm} &= T_{\mu \nu} m^{\mu} l^{\nu}, \\
T_{mm} &= T_{\mu \nu} m^{\mu} m^{\nu}, \\
T_{m^* m^*} &= T_{\mu \nu} m^{* \mu} m^{* \nu}. 
\end{split}
\end{equation}

\section{Derivation of the Teukolsky Equation}

Following the notation by Teukolsky, we write the gravitational perturbations to the Weyl scalars and the spinor coefficients as 
\begin{equation}
\begin{split}
\psi_0 &= \psi_0^{A} + \psi_0^{B}, \\
\psi_4 &= \psi_4^{A} + \psi_4^{B}, \\ 
\kappa &= \kappa^{A} + \kappa^{B}, \\
& .... 
\end{split}
\end{equation}
where the superscript of $ A $ denotes the unperturbed quantities and $B$ denotes the perturbation of these quantities. 
Since the Kerr spacetime is a Petrov-type-D spacetime, the following quantities vanish, 
\begin{equation}
\begin{split}
& \psi_{0}^{A}=\psi_{1}^{A}=\psi_{3}^{A}=\psi_{4}^{A}=0, \\
& \kappa^{A}=\sigma^{A}=\nu^{A}=\lambda^{A}=0. \\
\end{split}
\end{equation}
Substitute the perturbed Weyl scalars into the following Newman-Penrose equations, 
\begin{equation}
\begin{split}
& \left(\delta^{*}-4 \alpha+\pi\right) \psi_{0}-(D-4 \rho-2 \epsilon) \psi_{1}-3 \kappa \psi_{2}  -\left(D-2 \epsilon-2 \rho^{*}\right) \Phi_{01}+2 \sigma \Phi_{10} \\
& -2 \kappa \Phi_{11}-\kappa^{*} \Phi_{02}　=\left(\delta+\pi^{*}-2 \alpha^{*}-2 \beta\right) \Phi_{00}, \\
& (\Delta-4 \gamma+\mu) \psi_{0}-(\delta-4 \tau-2 \beta) \psi_{1}-3 \sigma \psi_{2} -\left(D-2 \epsilon+2 \epsilon^{*}-\rho^{*}\right) \Phi_{02}-\lambda^{*} \Phi_{00} \\
&+2 \sigma \Phi_{11}-2 \kappa \Phi_{12} = \left(\delta+2 \pi^{*}-2 \beta\right) \Phi_{01}, \\
& \left(D-\rho-\rho^{*}-3 \epsilon+\epsilon^{*}\right) \sigma-\left(\delta-\tau+\pi^{*}-\alpha^{*}-3 \beta\right) \kappa-\psi_{0}=0,  \\
\end{split}
\end{equation}
we have the perturbed Newman-Penrose equations, 
\begin{equation} \label{eq:perturbed_NP_equations}
\begin{split}
& (\delta^{*}-4 \alpha+\pi)^{A} \psi_{0}^{B}-(D-4 \rho-2 \epsilon)^{A} \psi_{1}^{B}-3 \kappa^{B} \psi_{2}^{A} \\ 
& =4 \pi[(\delta+\pi^{*}-2 \alpha^{*}-2 \beta)^{A} T_{ll}^{B} -(D-2 \epsilon-2 \rho^{*})^{A} T_{l m}^{B} ], \\
& (\Delta-4 \gamma+\mu)^{A} \psi_{0}^{B}-(\delta-4 \tau-2 \beta)^{A} \psi_{1}^{B}-3 \sigma^{B} \psi_{2}^{A}\\
& =4 \pi[(\delta+2 \pi^{*}-2 \beta)^{A} T_{l m}^{B} -(D-2 \epsilon+2 \epsilon^{*}-\rho^{*})^{A} T_{m m}^{B} ], \\
& (D-\rho-\rho^{*}-3 \epsilon+\epsilon^{*})^{A} \sigma^{B}-(\delta-\tau+\pi^{*}-\alpha^{*}-3 \beta)^{A} \kappa^{B}-\psi_{0}^{B}=0. 
\end{split}
\end{equation}
From now on, we drop the sign $ A $ for the unperturbed quantities. 
As for the Kerr metric, 
\begin{equation}
\begin{split}
D \psi_{2} &=3 \rho \psi_{2}, \\
\delta \psi_{2} &= 3 \tau \psi_{2}, 
\end{split}
\end{equation}
we have 
\begin{equation}
(D-3 \epsilon+\epsilon^{*}-4 \rho-\rho^{*}) \psi_{2} \sigma^{B}-(\delta+\pi^{*}-\alpha^{*}-3 \beta-4 \tau) \psi_{2} \kappa^{B}-\psi_{0}^{B} \psi_{2}=0. 
\end{equation}
To eliminate $ \psi_1^{B} $ \cref{eq:perturbed_NP_equations}, we make use of the following commutation relation \cite{Teukolsky_02_ApJ}, 
\begin{equation}
\begin{split}
& [D-(p+1) \epsilon+\epsilon^{*}+q \rho-\rho^{*}](\delta-p \beta+q \tau) \\
& - [\delta-(p+1) \beta-\alpha^{*}+\pi^{*}+q \tau](D-p \epsilon+q \rho)=0, \\
\end{split}
\end{equation}
which leads to 
\begin{equation}
\begin{split}
[(D-3 \epsilon+\epsilon^{*}&-4 \rho-\rho^{*} )(\Delta-4 \gamma+\mu) \\ &-(\delta+\pi^{*}-\alpha^{*}-3 \beta-4 \tau)(\delta^{*}+\pi-4 \alpha)-3 \psi_{2} ] \psi_{0}^{B}=4 \pi T_{0}, \\
[(\Delta+3 \gamma-\gamma^{*}&+4 \mu+\mu^{*})(D+4 \epsilon-\rho ) \\& -(\delta^{*}-\tau^{*}+\beta^{*}+3 \alpha+4 \pi)(\delta-\tau+4 \beta)-3 \psi_{2} ] \psi_{4}^{B} =4 \pi T_{4},  
\end{split}
\end{equation}
where 
\begin{equation}
\begin{split}
T_{0} &=(\delta+\pi^{*}-\alpha^{*}-3 \beta-4 \tau)[(D-2 \epsilon-2 \rho^{*}) T_{l m}^{B}-(\delta+\pi^{*}-2 \alpha^{*}-2 \beta) T_{u}^{B}] \\ &+(D-3 \epsilon+\epsilon^{*}-4 \rho-\rho^{*})[(\delta+2 \pi^{*}-2 \beta) T_{l m}^{B}-(D-2 \epsilon+2 \epsilon^{*}-\rho^{*}) T_{m m}^{B}], \\
T_{4} &=(\Delta+3 \gamma-\gamma^{*}+4 \mu+\mu^{*})[(\delta^{*}-2 \tau^{*}+2 \alpha) T_{n m *}-(\Delta+2 \gamma-2 \gamma^{*}+\mu^{*}) T_{m * m *}] \\ &+(\delta^{*}-\tau^{*}+\beta^{*}+3 \alpha+4 \pi)[(\Delta+2 \gamma+2 \mu^{*}) T_{n m *}-(\delta^{*}-\tau^{*}+2 \beta^{*}+2 \alpha) T_{n n}]. 
\end{split}
\end{equation}

% For gravitational perturbation in vacuum, $ T_0 = T_4 = 0 $. 
If we substitute the spinor coefficients for spacetime of rotating black holes, we obtained the Teukolsky equation, 
\begin{equation}\label{eq:TE}
\begin{split}
&\Bigg( \frac{(r^2 + a^2)^2}{\Delta} - a^2 \sin^2 \theta \Bigg) \frac{\partial^2 \psi}{\partial t^2} + \frac{4 Mar}{\Delta} \frac{\partial^2 \psi}{\partial t \partial \phi} + \Bigg( \frac{a^2}{\Delta} - \frac{1}{\sin^2 \theta} \Bigg) \frac{\partial^2 \psi}{\partial \phi^2} - \Delta^{-s} \frac{\partial }{\partial r} \Big( \Delta^{s+1} \frac{\partial \psi}{\partial r}\Big) \\
& - \frac{1}{\sin \theta} \frac{\partial }{\partial \theta} \Big( \sin \theta \frac{\partial \psi}{\partial \theta} \Big) - 2s\Bigg( \frac{a(r-M)}{\Delta} + i \frac{\cos \theta}{\sin^2 \theta} \Bigg)\frac{\partial \psi}{\partial \phi} - 2s \Bigg(\frac{M(r^2 - a^2)}{\Delta} - r - ia \cos \theta \Bigg) \frac{\partial \psi}{\partial t} \\
& +(s^2 \cot^2 \theta - s) \psi = T,    
\end{split}
\end{equation}
where $ s $ is the spin weight (or helicity) of the perturbation field and $ T　$ is the source term of the Teukolsky equation. 
For ingoing gravitational perturbations, $ s = 2$, $ \psi =\psi_0$ and $ T = 2 T_0 $. 
For outgoing gravitational perturbations, $ s = -2 $, $ \psi = \rho^{-4} \psi_4, T = 2 \rho^{-4} T_4$. 
Note that $ \Delta $ in \cref{eq:TE} is not the directional derivative defined in the above section. 
Instead, $ \Delta = (r - r_-)(r - r_+) $ where $ r_- $ and $ r_+ $ are the inner and outer event horizon of the rotating black hole. 

\section{Energy Carried by the Ringdown Waveform}

During the ringdown phase, gravitational waves are purely outgoing at spatial infinity and carrying energy away from the perturbed black hole. 
We first recall from Chapter 1, the power carried by gravitational waves is proportional to $ \braket{\dot{h}_{+}^{2} + \dot{h}_{\times}^2} $. 
We also recall that in the far-field limit, $ \psi \rightarrow \ddot{h}_{+} - i \ddot{h}_{\times} $. 
Thus, the power carried by gravitational waves during the ringdown phase can be expressed as \cite{Teukolsky_02_ApJ}
\begin{equation}
\frac{d^2 E}{d t d \Omega} \sim r^2 \braket{\dot{h}_{+}^2 + \dot{h}_{\times}^2} = \frac{r^2}{\omega^2}\braker{(\ddot{h}_{+} - i \ddot{h}_{\times})(\ddot{h}_{+} + i \ddot{h}_{\times})} = \frac{r^2 }{\omega^2} \psi_{4} \psi_{4}^{\dagger} = \frac{r^2 }{\omega^2} |\psi_{4}|^2,   
\end{equation}
where $ d \Omega $ is the element of solid angle. 
Turns out that above analysis misses a factor of $ \frac{1}{4 \pi} $. 
Putting the relevant physical constants, the energy out flux of the ringdown waves to spatial infinity is given by 
\begin{equation}
\frac{d^2 E}{d t d \Omega} = \lim _{r \rightarrow \infty} \frac{r^{2}}{4 \pi \omega^{2}}\left|\psi_{4} \right|^{2}. 
\end{equation}

\section{Properties of the Teukolsky Equation}

\subsection{Separation of Variables}

The Teukolsky equation is a separable second-order linear partial differential equation. 
By letting 
\begin{equation}\label{eq:TE_Radial}
\psi = R(r) S_{lm} (\theta, \phi) e^{- i \omega t}, 
\end{equation}
where $ m $ is the magnetic quantum number of the wave function $ \psi$, \cref{eq:TE} is split into two ordinary differential equations. 

\subsubsection{The Radial Equation}

The first is the radial equation, 
\begin{equation}
\Delta^{-s} \frac{d}{d r}\left(\Delta^{s+1} \frac{d R}{d r}\right)+\left(\frac{K^{2}-2 i s(r-M) K}{\Delta}+4 i s \omega r-\lambda\right) R=0, 
\end{equation}
where $ \lambda $ is the separation constant, a function of the orbital quantum number $ l $. 

\noindent \textbf{Analytic Solution in terms of Hypergeometric Functions}

Note that the radial equation is regularly singular at $ r = r_{\pm} $ and irregularly singular at $ r = \infty $. 
An ordinary differential equation with these singularities can be transformed to a hypergeometric equation by the following transformations \cite{exact_sol_TE}. 
We first let, $ \epsilon=2 M \omega, \kappa=\sqrt{1-a^{2}}, \tau=\frac{\epsilon-m a}{\kappa} $ and $ x=\omega\left(r_{+}-r\right) / \epsilon \kappa $. 
Then, if we let, 
\begin{equation}
R(x)  =e^{i \epsilon \kappa x}(-x)^{-s-i \frac{\epsilon+\tau}{2}}(1-x)^{i \frac{\epsilon-\tau}{2}} p(x), 
\end{equation} 
then $ p(x) $ satisfies the following hypergeometric equation, 
\begin{equation} \label{eq:hypergeometric_equation}
\begin{split}
& x(1-x) \frac{d^2 p}{dx^2} + \big( 1-s-i \epsilon-i \tau-(2-2 i \tau) x \big) \frac{d p}{dx } \\
& = 2 i \epsilon \kappa \big(-x(1-x) \frac{d p}{dx } +(1-s+i \epsilon-i \tau) x p \big) \\
& +\left[-\lambda-s(s+1)+\nu(\nu+1)+\epsilon^{2}-i \epsilon \kappa(1-2 s)\right] p,  
\end{split}
\end{equation}
where $ \nu $ is the renormalized angular momentum, 
\begin{equation}
\nu=l+\frac{1}{2 l+1}\left[-2-\frac{s^{2}}{l(l+1)}+\frac{\left[(l+1)^{2}-s^{2}\right]^{2}}{(2 l+1)(2 l+2)(2 l+3)}-\frac{\left(l^{2}-s^{2}\right)^{2}}{(2 l-1) 2 l(2 l+1)}\right] \epsilon^{2}+O\left(\epsilon^{3}\right), 
\end{equation}
$ \lambda $ is the separation constant, 
\begin{equation}
\begin{split}
\lambda = &  E-s(s+1)-2 m M a \omega+M^2 a^{2} \omega^{2} ,\\
E = & l(l+1)-\frac{2 s^{2} m \xi}{l(l+1)}+[H(l+1)-H(l)-1] \xi^{2}+O\left(\xi^{3}\right), \\
H(l) = & \frac{2\left(l^{2}-m^{2}\right)\left(l^{2}-s^{2}\right)^{2}}{(2 l-1) l^{3}(2 l+1)}, 
\end{split}
\end{equation}
and $ \xi = M a \omega $. 

\cref{eq:hypergeometric_equation} permits solutions in series of hypergeometric functions, 
\begin{equation}
p (x)=\sum_{n=-\infty}^{\infty} a_{n}^{\nu} p_{n+\nu}(x), 
\end{equation}
where
\begin{equation}\label{eq:hypergeometric_series}
p_{n+\nu}(x)=F(n+\nu+1-i \tau,-n-\nu-i \tau ; 1-s-i \epsilon-i \tau ; x),  
\end{equation}
and $ F $ is hypergeometric function. 
The coefficients $ a_{n}^{\nu} $ ($\nu$ does not stand for power) obey a three-term recurrence relation, which is obtained by substitute \cref{eq:hypergeometric_series} into \cref{eq:hypergeometric_equation}, 
\begin{equation} \label{eq:recurrence}
\alpha_{n}^{\nu} a_{n+1}^{\nu}+\beta_{n}^{\nu} a_{n}^{\nu}+\gamma_{n}^{\nu} a_{n-1}^{\nu}=0, 
\end{equation}
where 
\begin{equation}
\begin{split}
\alpha_{n}^{\nu} =& \frac{i \epsilon \kappa(n+\nu+1+s+i \epsilon)(n+\nu+1+s-i \epsilon)(n+\nu+1+i \tau)}{(n+\nu+1)(2 n+2 \nu+3)} \\ 
\beta_{n}^{\nu} =&-\lambda-s(s+1)+(n+\nu)(n+\nu+1)+\epsilon^{2}+\epsilon(\epsilon-m a) +\frac{\epsilon(\epsilon-m a)\left(s^{2}+\epsilon^{2}\right)}{(n+\nu)(n+\nu+1)} \\ 
\gamma_{n}^{\nu}=&-\frac{i \epsilon \kappa(n+\nu-s+i \epsilon)(n+\nu-s-i \epsilon)(n+\nu-i \tau)}{(n+\nu)(2 n+2 \nu-1)}.
\end{split}
\end{equation}
In particular, for large $ n $, 
\begin{equation}
\lim _{n \rightarrow \infty} n \frac{a_{n}^{\nu}}{a_{n-1}^{\nu}}=-\lim _{n \rightarrow-\infty} n \frac{a_{n}^{\nu}}{a_{n+1}^{\nu}}=\frac{i \epsilon \kappa}{2}. 
\end{equation}

$ a_{n}^{\nu} $ can be solved from \cref{eq:hypergeometric_series} upon a suitable choice of initial condition. 
Following \cite{exact_sol_TE}, $ a_{0}^{\nu} $ can be chosen to be $ 1 $. 
In particular, at the low-frequency limit ($\epsilon = 2 M \omega << 1 $), $ a_{\pm 1} $ and $ a_{\pm 2} $ can be solved as power series of $ \epsilon $, 
\begin{equation}
\begin{aligned} 
a_{1}^{\nu}=& i \frac{(l+1-s)^{2}[(l+1) \kappa+i m a]}{2(l+1)^{2}(2 l+1)} \epsilon +\frac{(l+1-s)^{2}}{2(l+1)^{2}(2 l+1)}\left[1-i \frac{(l+1) \kappa+i m a}{l(l+1)^{2}(l+2)} m a s^{2}\right] \epsilon^{2}+O\left(\epsilon^{3}\right), 
\end{aligned}
\end{equation}
\begin{equation}
\begin{aligned} 
a_{-1}^{\nu}=& i \frac{(l+s)^{2}[l \kappa-i m a]}{2 l^{2}(2 l+1)} \epsilon -\frac{(l+s)^{2}}{2 l^{2}(2 l+1)}\left[1+i \frac{l \kappa-i m a}{(l-1) l^{2}(l+1)} m a s^{2}\right] \epsilon^{2}+O\left(\epsilon^{3}\right), 
\end{aligned}
\end{equation}
\begin{equation}
a_{2}^{\nu}=-\frac{(l+1-s)^{2}(l+2-s)^{2}[(l+1) \kappa+i m a][(l+2) \kappa+i m a]}{4(l+1)^{2}(l+2)(2 l+1)(2 l+3)^{2}} \epsilon^{2}+O\left(\epsilon^{3}\right), 
\end{equation}
\begin{equation}
a_{-2}^{\nu}=-\frac{(l-1+s)^{2}(l+s)^{2}[(l-1) \kappa-i m a][l \kappa-i m a]}{4(l-1) l^{2}(2 l-1)^{2}(2 l+1)} \epsilon^{2}+O\left(\epsilon^{3}\right). 
\end{equation}

\noindent \textbf{The Form of the Klein-Gordon Equation}

Similar to the Regge-Wheeler and Zerilli equation, the radial Teukolsky equation can also be brought into the form of the Klein-Gordon equation. 
If we let $ u = \sqrt{r^{2}+a^{2}} \Delta^{s / 2} R $ \cite{Nakamura_01}, the radial Teukolsky equation \cref{eq:TE_Radial} can be transformed into the form of Klein-Gordon equation,
\begin{equation} \label{eq:KG_TE}
\frac{\partial^2 u}{\partial x^2 } + \Big( \omega^2 - V(r) \Big) u = 0, 
\end{equation}
where $ x $ is the tortoise coordinate, 
\begin{equation}
\frac{d x}{d r} = \frac{r^2 + M^2 a^2}{\Delta}, 
\end{equation}
and $ V(r) $ is the effective potential for gravitational-wave propagation, 
\begin{equation}
\begin{split}
V(\omega, r)=& -\frac{2 i s \omega r}{r^{2}+a^{2}}+\frac{\lambda+s+s^{2}+2 a m \omega+6 M i s \omega}{r^{2}+a^{2}}+\frac{[2 M(1-\lambda-s-s^{2})-2 i a m s] r}{r^{2}+a^{2}} \\ 
& +\frac{a^{2}-4 M^{2}-a^{2} m^{2}+2 M i a m s+M^{2} s^{2}-a^{2} s^{2}-8 M i a^{2} s \omega}{(r^{2}+a^{2})^{2}} \\
& -\frac{4 a^{2} r}{\left(r^{2}+a^{2}\right)^{3}}+\frac{4 a^{2}}{\left(r^{2}+a^{2}\right)^{3}}-\frac{3 a^{4}}{\left(r^{2}+a^{2}\right)^{4}}. 
\end{split}
\end{equation}
As we will see in the coming chapters, the Klein-Gordon form of the Teukolsky equation \cref{eq:KG_TE} will be more useful for studying black holes in alternative gravity theories. 

The radial Teukolsky equation of the Klein-Gordon form admits two linearly independent solutions. 
Each of these solutions corresponds to different asymptotic behaviour at the event horizon and spatial infinity. 
The first solution is the down mode, 
\begin{equation}\label{eq:down_mode}
\tilde{u}_{\rm down} (x, \omega) \approx 
\begin{cases}
& \Delta^{-s/2} e^{- i \omega x}, ~ x \rightarrow r_+ \\
& A_1 (\omega) r^{+s} e^{- i \omega r} + A_2(\omega) r^{-s} e^{i \omega r} , ~ x \rightarrow + \infty \\ 
\end{cases}
\end{equation}
which is purely ingoing at the event horizon and the up mode 
% When $ \mathcal{I} = 0 $, \ref{eq:SE} admits two linearly independent solutions: the down mode, 
\begin{equation}\label{eq:up_mode}
\tilde{u}_{\rm up} (x, \omega) \approx 
\begin{cases}
& B_1(\omega) \Delta^{-s/2} e^{-i \omega x} + B_2 (\omega) \Delta^{s/2} e^{i \omega x}, ~ x \rightarrow r_+ \\
& r^{-s} e^{i \omega r} , ~ x \rightarrow + \infty \\ 
\end{cases}
\end{equation}
which is purely outgoing at spatial infinity, where $ A_1(\omega) $, $A_2(\omega) $, $ B_1 (\omega) $ and $ B_2 (\omega)$ are functions of $ \omega$. 
In particular, $ A_1(\omega) $ and $ B_2 (\omega)$ are proportional to the Wronskian of the differential equation.

\subsubsection{The Angular Equation}

$ S(\theta) $ obeys the angular equation, 
\begin{equation}\label{eq:angular_equation}
\left\{\frac{1}{\sin \theta} \frac{d}{d \theta}\left[\sin \theta \frac{d}{d \theta}\right]-a^{2} \omega^{2} \sin ^{2} \theta-\frac{(m+s \cos \theta)^{2}}{\sin ^{2} \theta}-2 a \omega s \cos \theta+s+2 m a \omega+\lambda\right\} {}_{s} S_{\ell m}=0, 
\end{equation}
% We denote the solution to the angular Teukolsky equation corresponding to the orbital quantum number $ l $ by $ S_{l}(\theta) $ and more compactly 
% \begin{equation}\label{eq:up_mode_aysmtop}
% S_{lm} (\theta, \phi) = S_{l} (\theta) e^{i m \phi}. 
% \end{equation}
$ S_{lm} (\theta, \phi) $ are the spin-weighted spheroidal harmonics.
Leaver found \cite{Leaver_02} that the solution to \cref{eq:angular_equation} can be expressed in the form of 
\begin{equation}
_s S_{\ell m n}(\theta, \phi)=e^{i m \phi} e^{c x}(1+\cos \theta)^{k_-}(1-\cos \theta)^{k_+} \sum_{p=0}^{\infty} a_{p}(1+\cos \theta)^{p}, 
\end{equation} 
where $ c = M a \omega $, $k_{ \pm} \equiv|m \pm s| / 2$ and $ a_p $ obey the three-term recursion relation of \cite{Leaver_02} 
\begin{equation}
\begin{aligned} 
\alpha_{0} a_{1}+\beta_{0} a_{0} &=0 \\ 
\alpha_{p} a_{p+1}+\beta_{p} a_{p}+\gamma_{p} a_{p-1} &=0, \quad p=1,2 \ldots,  
\end{aligned}
\end{equation}
with 
\begin{equation}
\begin{aligned} 
\alpha_{p} &=-2(p+1)\left(p+2 k_{-}+1\right), \\ 
\beta_{p} &=p(p-1)+2 p\left(k_{-}+k_{+}+1-2 c\right), \\ 
&-\left[2 c\left(2 k_{-}+s+1\right)-\left(k_{-}+k_{+}\right)\left(k_{-}+k_{+}+1\right)\right]-\left[c^{2}+s(s+1)+_{s} A_{l m}\right] \\ \gamma_{p} &=2 c\left(p+k_{-}+k_{+}+s\right). 
\end{aligned}
\end{equation}

Unlike the usual spherical harmonics $ Y_{lm} (\theta, \phi) $, which are orthogonal, spin-weighted spheroidal harmonics are not orthogonal, 
\begin{equation} \label{eq:inner_product_S}
\int d \Omega S_{lm} (\theta, \phi) S_{l' m'} (\theta, \phi) \neq \delta_{l l'}\delta_{m m'}. 
\end{equation}
In fact, the above inner product \cref{eq:inner_product_S} is complex and depends on the spin of the black hole. 
E. Berti \textit{et al.} have evaluated the \cref{eq:inner_product_S} and the eigenvalue $ A $ as complex functions of $ l $, $ m $ and $ a $ \cite{Berti_01}. 

\subsection{In Connection with Numerical-relativity Simulations}

Before numerical relativity had been well developed, the Teukolsky equation was an important tool to study astrophysical black holes subjected to gravitational perturbations. 
The Teukolsky equation was solved to compute the quasinormal-mode spectrum of rotating black holes by different numerical methods. 
The quasinormal-mode spectrum suggests that rotating black holes are stable against gravitational perturbations \cite{Teukolsky_03_ApJ}. 
These discoveries are important for our understanding of the final black hole formed in gravitational-wave events. 

An alternative way to solve the Teukolsky equation is to simulate the ringdown phase of different binary black-hole coalescences.
On top of quasinormal-mode frequencies, these simulations also reveal the mode components for the quasinormal-mode composition of ringdown waveforms. 
Numerical-relativity simulations suggest that the ringdown waveform of a final black hole due to binary black-hole coalescence can be fitted as 
\begin{equation}\label{eq:RD_WF}
\psi_4(r, t, \theta, \phi) = \frac{M}{r} \sum_{nlm} \tilde{\omega}_{nlm}^2 A_{nlm}(\eta, \chi) S_{lm} (\theta) e^{im \phi} e^{-i \omega t}, 
\end{equation}
where $ \theta $ is the angle between the line of sight and the spin of the black hole, $ \phi$ is the azimuthal angle, $ A_{nlm}(\eta, \chi) $ are functions of the symmetric mass ratio, $\eta$, and spins $ \chi$ of the parental binary black holes. 
The explicit expression of $ A_{nlm} $ can be found on \cite{Nikhef_01}. 
The ringdown waveform \cref{eq:RD_WF} is very important for parameter estimation of the final black hole in gravitational-wave events. 

\subsection{Other Useful Properties}

Besides the properties stated above, the Teukolsky equation also possesses the following properties. 

Firstly, when scalar perturbations ($\Phi$) are considered, the Teukolsky equation reduces to the scalar wave equation in Kerr metric, 
\begin{equation}
\begin{split}
 (r^2 + a^2 \sin^2 \theta) \nabla_{\mu} \nabla^{\mu} \Phi = 0,  
\end{split}
\end{equation}
where $ \nabla^{\mu} $ is the covariant derivative with defined with respect to the Kerr spacetime. 
This corresponds to the equation of motion of a massless field in the Kerr spacetime. 
Thus, for $ s = 0 $, the Teukolsky equation describes massless scalar perturbations to black holes. 

Secondly, observed that, in the far field limit, the Teukolsky equation becomes, 
\begin{equation}
r^2 \Big( \frac{\partial^2 \psi}{\partial t^2} - \frac{\partial^2 \psi}{\partial r^2}\Big) = 0, 
\end{equation}
for whatever $ s $. 
This equation corresponds to the dispersion of $ k^{\mu} k_{\mu} = 0 $ in the far-field limit. 
Thus, at least to far-field observers, the Teukolsky equation is an equation describing massless perturbation fields. 

These two properties are important to test general relativity through black-hole ringdown detection. 
As far as gravitational perturbations are concerned, these two are manifestations of the fact that gravitational waves obey the dispersion of $ \omega = k $. 
For the Teukolsky equation for black holes in alternative gravity theories, for example, theories predicting alternative dispersion relation of gravitational waves, the far-field limit of the Teukolsky equation should be different. 
These suggest that modifications are needed to incorporate rotating black holes in alternative gravity theories. 
These properties also offer us hints of how to modify the Teukolsky equation, which will be explored in the coming chapters. 

\section{Conclusions}

In this chapter, we review the gravitational perturbations of rotating black holes. 
In particular, we derive the Teukolsky equation based on the spinor-coefficient formalism. 
The Teukolsky equation is a convenient tool to study gravitational perturbations of astrophysical black holes, as it is a separable second-order linear partial differential equation governing the gravitational perturbations of arbitrarily spinning black holes. 
By separation of variables, the radial part and the angular part of the gravitational perturbations separately satisfy an ordinary differential equation. 
By solving the ordinary differential equations, quasinormal-mode frequencies and the ringdown phase of astrophysical black holes with rotation can be studied. 

Although the Teukolsky equation is a convenient tool to study gravitational perturbations of rotating black holes, it only describes massless-perturbation fields. 
Gravitational waves in alternative gravity theories might obey different dispersion relation. 
In principle, one can construct the null tetrads satisfying the cross normalization conditions for a rotating black hole in alternative theories. 
However, it is difficult to derive the metric describing a rotating black hole in alternative theories. 
Therefore, to derive the Teukolsky equation for rotating black holes for different alternative theories, separate treatments are needed. 
In the coming chapter, we will derive the Teukolsky equation for gravitational perturbations with phenomenological graviton mass of rotating black holes. 
