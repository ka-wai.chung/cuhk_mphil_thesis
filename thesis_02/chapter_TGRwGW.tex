\chapter{Testing General Relativity through Gravitational-wave Detection} 

\section{Introduction}

In this chapter, we begin by first introducing some "classical" \footnote{We generally refer "classical tests" to the tests carried out before the direct detection of gravitational waves.} tests of general relativity.
We then proceed to introduce several examples of tests of general relativity through gravitational-wave detection.  
Since general relativity has passed all the tests, it has been regarded as the most adequate theory describing gravity. 
However, as positive results piled up, another problem is posed for physicists: what are the limitations of general relativity?
The motivation to know its limitations of calls for more new tests of general relativity. 

\section{Classical Tests of General Relativity}

\subsection{The Advance of Perihelion of Mercury}

The first attempt to justify the validity of general relativity is to explain the anomalous advance of the perihelion of Mercury.
After decades of observations, astronomers had concluded that the Newtonian predictions of the advance rate of Mercury's perihelion due to the perturbations by other known celestial objects within the solar system (575.31'per century)\cite{Mercury_02} does not fit with observations (574.10'±0.65 per century)\cite{Mercury_01}. 
There is a disagreement of $\sim$ 45" $\pm$ 5". 
To address this discrepancy, astronomers started to look for a new planet which was postulated to be more nearer to the Sun than Mercury.
Nonetheless, astronomers could not find any nearer planet.

When developing general relativity, Einstein applied his general relativity to compute Mercury's orbit. 
By solving his field equation through perturbation \cite{Einstein:1915bz}, Einstein computed that Mercury's perihelion should be advancing at a rate of 
\begin{equation}
\Delta = \frac{24 \pi^3 a^2}{T^2 c^2 (1-e^2)}, 
\end{equation} 
where $ a $ is the semi-major axis, $ T $ is the orbital period and $ e $ is the eccentricity of the orbit.
For Mercury's orbit, the above equation gives $\sim$ 43", which is in good agreement with observation, which provided the first hint that general relativity is correct.  

\subsection{The Eddington Experiment: Testing the Equivalence Principle by Observing Bending of Light Rays}

The next test concern another prediction by general relativity: light will be deflected by gravitation field (first predicted by Einstein with Newtonian spirit, see \cite{Einstein:1911vc}).
According to special relativity, energy is equivalent to inertial mass. 
According to the equivalence principle, inertial mass is equivalent to gravitational mass. 
Therefore, a photon propagating around a star will change its direction of propagation because it carries energy which is equivalent to gravitational mass. 
With non zero gravitational mass, photon will be deflected like a massive particle around the star. 
This phenomenon is actually a simple case of gravitational lensing. 

However, solely observing that light deflection is not unique evidence of general relativity.
Newtonian gravity also permits light deflection except with different bending angle.   
If a photon is beaming to a star at an impact parameter $ b \gg M $, Newtonian gravity predicts that the bending angle of a light ray passing by along the star to be 
\begin{equation}
\alpha \sim \frac{2GM}{c^2 b}, 
\end{equation}
where $ \alpha $ is the deflection angle and $ M $ is the mass of the star. 
On the other hand, general relativity predicts that 
\begin{equation}
\alpha \sim \frac{4GM}{c^2 b},
\end{equation}
which is the twice of the prediction by Newtonian gravity.
At the surface of the sun, Newtonian gravity predicts a deflection of $ 0.865" $ and general relativity predicts $ 1.75" $. 
Therefore, to really distil the correct theory, astronomers need to measure the light deflection precisely. 

Within the solar system, the Sun is the most ideal candidate (after all, it is the most massive object in the solar system) to observe for light deflection.  
To test general relativity, Sir Arthur Eddington carried out the first attempt to accurately measure light deflection by the Sun in 1919. 
Eddington measured the sky positions of stars of Hyades around the Sun during a total solar eclipse on May 29, 1919. 
By comparing the original stellar positions and that during the eclipse, Eddington measured that the light deflection of light rays due to the stars were about $ 1.98" (\pm) 0.12" $, which favoured the prediction by general relativity \cite{Eddington}. 
This test provide the second evidence to support general relativity over Newtonian gravity. 
After this confirmation, people star to believe the validity of general relativity. 

\subsection{Cassini Tests of General Relativity}

Because of its rings, Saturn has been one of the most fascinating planets which captures the interests of astronomers.
In 1997, a space probe, named "Cassini-Huygens", was sent to Saturn for more comprehensive studies about the structures and properties of Saturn and its rings.
In 2002, the Earth, Sun and Cassini-Huygens began to align.
Astronomers made use of the alignment to carry out an experiment to test gravitational redshift, a phenomenon predicted by general relativity.

During the close alignment, the radio signal sent from the Earth and transmitted back by Cassini-Huygens will be subjected to gravitational redshift due to the Sun, as predicted by general relativity. 
According to general relativity, the fractional frequency change of the radio signal due to gravitational redshift should be \cite{Cassini_02} 
\begin{equation}
\frac{\Delta \nu}{\nu} = -2(1+\gamma) \frac{G M_{\odot}}{c^{3} b} \frac{\mathrm{d} b}{\mathrm{d} t}=-\left(1 \times 10^{-5} \mathrm{s}\right)(1+\gamma) \frac{1}{b} \frac{\mathrm{d} b}{\mathrm{d} t},  
\end{equation}
where $ b $ is the impact parameter, $ M_{\odot} $ is the solar mass, $ \gamma $ is a (dimensionless) post-Newtonian parameter which should exactly be one if general relativity is correct. 
By measuring the frequency shift of radio signal transmitted by Cassini-Huygens, $ \gamma $ was constrained that \cite{Cassini_01} 
\begin{equation}
\gamma=1+(2.1 \pm 2.3) \times 10^{-5}, 
\end{equation}
which is a result highly consistent with the prediction by general relativity. 

\subsection{Hulse–Taylor Binary: Indirect Evidence of Gravitational Waves}

Prior to the first direct detection of gravitational waves, physicists and astronomers had been trying for decades to detect them directly. 
On the other hand, some astronomers took indirect approaches to look for indirect ways to confirm the existence of gravitational waves. 

Measuring the period of binary compact objects, such as pulsar or neutron star might be a possible way. 
According to our calculations in Chapter 1, a compact orbital binary emits gravitational waves. 
As a consequence that the generated gravitational waves transmit energy away from the system, the orbital radius of the compact binary will shrink. 
Specifically, according to general relativity, the shrink or the collapse of the compact binary can be characterized by the rate of period decreasing per unit time, which is given by \cite{Peters_and_Mathews}, 
\begin{equation}\label{eq:Peters_and_Mathews}
\begin{aligned}
\dot{T} &=-\frac{192 \pi G^{5 / 3}}{5 c^{5}}\left(\frac{T}{2 \pi}\right)^{-5 / 3}\left(1-e^{2}\right)^{-7 / 2} \left(1+\frac{73}{24} e^{2}+\frac{37}{96} e^{4}\right) m_{1} m_{2}\left(m_{1}+m_{2}\right)^{-1 / 3}, 
\end{aligned}
\end{equation}
where $ T $ is the period, $ m_1 $ and $ m_2 $ are the masses of the binaries and $ e $ is the eccentricity. 
Therefore, observation of period reduction of a compact binary tests the validity of general relativity and the existence of gravitational waves. 

In 1974,  Russell Hulse and Joseph Taylor discovered a binary (PSR J1915+1606, or more commonly known as the "Hulse–Taylor Binary") consisting of a pulsar and a neutron star. 
Then, by prolonged observations of PSR J1915+1606, they measured its orbital period. 
They found that the orbital period of PSR J1915+1606 is decreasing at a rate highly consistent with the prediction by general relativity \cite{Hulse_Taylor_01, Hulse_Taylor_02} (see \cref{fig:HT_Binary}).
This remarkable test not only once again confirmed the validity of general relativity but also provide the first indirect evidence for the existence of gravitational waves.  

\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{HT_Binary.pdf}
  \caption{The orbital period of the Hulse–Taylor binary as a function of time. Hulse and Taylor measured the period of the Hulse–Taylor binary in 30 years (denoted by dots). The measured period has been decreasing in good consistency with the prediction of general relativity (the solid line). Note that error bars have been included in the figure. }
  \label{fig:HT_Binary}
\end{figure}


\iffalse

\section{The Art of Testing General Relativity Through Gravitational-wave Detection}

Detection of gravitational waves provides us a new novel way to test general relativity. 
The behaviour and the resultant gravitational waves due to compact binary coalescences can be uniquely determined by general relativity. 
Thus, any deviation from prediction indicates the break down of general relativity. 
If we can parameterize deviation of gravitational waves detected from the prediction, we can estimate the posterior of the deviation through the procedure outlined in \cref{sec:PE} and thereby be a test of general relativity and the framework of Bayesian inference developed in the last chapter. 

\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{TGR_Flowchart.pdf}
  \caption{The flow chart which sketches the pathway when designing a new test of general relativity through gravitational-wave detection.}
  \label{fig:flow_chart}
\end{figure}
\cref{fig:flow_chart} sketches the pathway of designing a new test of general relativity include the following pathway, 
\begin{enumerate}
\item Consider a motivated correction of general relativity. 
For the sake of your survival in academia, you are advised to consider a really motivated correction of general relativity. 
Then derive the modification to gravitational waveforms due to the correction considered. 
For example, this thesis focuses on the correction due to Lorentz violation and quantum gravity, which are popular alternative gravity theories. 
\item Work out the effects of the considered corrections on gravitational-wave generation.
In particular, this thesis focuses on the generation of gravitational waves during the ringdown phase. 
Unless with reasonable justification \footnote{For example, the vDVZ discontinuity in massive gravity theories \cite{Massive_Gravity_01, Massive_Gravity_02}, which is a discontinuity fail to reduce to general relativity when consider the limit of zero graviton mass}, as a rule of thumb, when alternative physics is switched off, the waveforms should reduce to the known prediction by general relativity. 
\item Parameterize the modification. 
We must first check that the parametrization must not be degenerate with the effects due to other existing parameters, for example, masses and spins. 
Otherwise, we must look for an alternative parametrization which can free us from the degeneracy, or the whole test could have been scrapped as we cannot really tell if the changes of waveforms are due to alternative physics or other intrinsic properties of the source.
For example, when exploring the signature of black-hole radiation in ringdown, if we parameterize the radiation signature by the amplitude of the radiation, then the posterior will be degenerate with the luminosity distance.
To escape from this degeneracy, we parameterize the temperature instead.   
Degeneracy can be checked by studying similarity between waveforms in general relativity and those with parameterized modification. 
\item Implement the modification and register the violation parameters into existing pipelines for posterior estimation. 
There are some tips concerning the registration of the parameters. 
If the parameter is one-sided, for example, mass of some particles or temperature, which only permitted for positive number and values of the concerned parameter should be zero, as predicted by general relativity, then it will be more convenient if the parameter is inferred in decade form, i.e. Inferring the $ \log_{10} $ of the values instead of the values. 
\end{enumerate}

\fi 

\section{Tests of General Relativity with Direct Gravitational Waves} \footnote{Largely based on \cite{LIGO_07}. }

As we can see, before the first direct detection of gravitational waves, most of the tests of general relativity were performed within the solar system or regimes of relatively weak field. 
This weakness limits the scope of these tests of general relativity: the validity of general relativity in a strong field regime remains to be tested. 
Massive compact objects, for example, solar-mass black holes are ideal for strong-field tests. 
However, solar-mass binary black holes cannot be accessed through observations of electromagnetic channels. 
Thanks to the ground-based gravitational-wave detector network, not only we can study the behaviour of black holes through direct detection of gravitational waves, but we can also extensively testing general relativity in the strong-field regime where black holes present. 
In this section, we will briefly introduce a number of examples of widely employed tests of general relativity through gravitational-wave detections. 

\subsection{Post Newtonian Parameterized Test}

A popular aspect of testing general relativity is to look at the deviation of gravitational-wave generation by compact binaries. 
In order to test general relativity in model-independent way, we parameterize the deviation of the orbital phase from the prediction by general relativity. 
The orbital phase of compact binary coalescences predicted by general relativity can be described by \cite{TIGER}
\begin{equation}
\Psi(f)=2 \pi f t_{c}-\varphi_{c}-\frac{\pi}{4}+\sum_{j=0}^{7}\left[\psi_{j}+\psi_{j}^{(l)} \ln f\right] f^{(j-5) / 3}, 
\end{equation}
where $ t_c $ and $ \vphi_c $ are the time and phase of coalescences, $ \psi_{j} $ (and $ \psi_{j}^{(l)}$) are the post Newtonian coefficients. 
Post Newtonian Parameterized Test considers the following modification, 
\begin{equation}
\psi_{j} \rightarrow \psi_{j}(1+ \delta \psi_{j}). 
\end{equation}
If detection is consistent with general relativity, the obtained posteriors of $ \delta \psi_{j} $ should be central at $ \delta \psi_{j} = 0 $. 
So far, we have not detected a significant deviation of $ \psi $ from the prediction by general relativity\cite{LIGO_07, LIGO_11}. 

\subsection{Inspiral-merger-ringdown (IMR) Consistency Test}

For a binary black hole of given parental masses and spins, general relativity uniquely predicts the mass and spin of the final black hole. 
Through detecting gravitational waves, the masses and spins of parental black holes can be measured by the orbital phase of the inspiral stage. 
Similarly, the mass and spin of final black holes can independently be measured by measuring the ringdown frequencies, provided that the signal-to-noise ratio of the ringdown signal is enough. 
% On the other hand, for a set of parent black holes of given masses and spins, general relativity uniquely predicts the mass and spin of the final black holes. 
Therefore, comparing the measured final mass to that predicted by general relativity provides us with an independent consistency test of general relativity \cite{IMR_Test_01, IMR_Test_02, IMR_Test_03}. 
This test is known as the inspiral-merger-ringdown (IMR) consistency test. 

Suppose we detected a gravitational waves signal. 
By Bayesian parameter estimation (see \cref{chap:Bayesian}), we obtain the posteriors of the parental masses and spins, $ M_1, M_2, \vec{\chi}_1 $ and $ \vec{\chi}_2 $. 
These posteriors of $ M_1, M_2, \vec{\chi}_1 $ and $ \vec{\chi}_2 $ imply a range of final mass and spin according to general relativity. 
We denote the posterior of predicted final mass and spin by $ p_{\rm inspiral} (M_f, \vec{\chi}_f) $ is the posterior of $ M_f $ and $ \vec{\chi}_f $. 
\textit{Independently}, the merger-ringdown signal gives us the posterior of the final mass and spin, $ M_f$ and $ \vec{\chi}_f $, $ p_{\rm merger-ringdown} (M_f, \vec{\chi}_f) $. 
Then, quantitative, we can measure the consistency between measurement and the prediction by general relativity by considering the overlap between the posteriors \cite{IMR_Test_01}, 
\begin{equation}
\begin{split}
p(\Delta M, \Delta \vec{\chi}_f) = \int d M_f d \vec{\chi}_f p_{\rm inspiral} (M_f, \vec{\chi}_f) p_{\rm merger-ringdown} (M_f - \Delta M_f, \vec{\chi}_f - \Delta \vec{\chi}_f), 
\end{split}
\end{equation}
where $ p_{\rm merger-ringdown} (M_f, \vec{\chi}_f) $ is the posterior of measured $ M_f $ and $ \vec{\chi}_f $ by analysing the data of the merger and ringdown phase, $ \Delta M_f $ and $ \Delta \vec{\chi}_f $ are the differences between the measured and predicted masses and spins. 

LIGO carried out this consistency test for the detection of GW150914 \cite{LIGO_07}. 
The top panel of \cref{fig:IMR_GW150914} plots the 90-\% confidence interval of the posteriors of $ M_f $ and the dimensionless aligned $ a_f $ by analysing the data (i) (pale violent dashed line) only the inspiral phase, (ii) (dark violent dashed line) the post-inspiral phase and (iii) (solid black line) using all the inspiral, merger and ringdown phase of GW150914. 
The bottom panel of \cref{fig:IMR_GW150914} plots the two-dimensional posterior $ p(\Delta M, \Delta a_f)  $ of GW150914. 
The dark blue solid line encloses the 90-\% confidence interval of $ p(\Delta M, \Delta a_f) $. 
From \cref{fig:IMR_GW150914}, we conclude that the final mass of GW150914 is consistent with the prediction by general relativity. 
\begin{figure}[htp!]
  \centering
  \includegraphics[scale=0.7]{IMR_GW150914.png}
  \caption{(Top panel) The 90-\% confidence interval of the posteriors of $ M_f $ and $ a_f $ by analysising the data (i) (pale violent dashed line) only the inspiral phase, (ii) (dark violent dashed line) the post-inspiral phase and (iii) (solid black line) using all the inspiral, merger and ringdown phase of GW150914. (Bottom panel) The two-dimensional posterior $ p(\Delta M, \Delta a_f)  $ of GW150914. The dark blue solid line encloses the 90-\% confidence interval of $ p(\Delta M, \Delta a_f) $. Figure reproduced from \cite{LIGO_07}. }
  \label{fig:IMR_GW150914}
\end{figure}


\subsection{Constraining the Mass of Graviton}

Phenologically, if graviton has mass, the dispersion relation of gravitational waves will be 
\begin{equation}
\omega^2 - k^2 = m_g^2, 
\end{equation}
where $ m_g $ is the phenomenological mass of graviton. 
A consequence of this modified dispersion relation is that gravitational waves of different frequencies will travel at different speeds. 
The differences of propagation velocities lead to dephasing of gravitational waves amongst different frequencies given by \cite{Wills_01, Wills_02, Ajith_01},  
\begin{equation}\label{eq:dephasing}
\delta \Psi(f) = - \frac{\pi D c}{\lambda_g^2 (1+z) f}, 
\end{equation}
where $ \lambda_g = \hbar / (m_g c) $ is the Compton's wavelength of the phenomenological graviton, $ D $ is the cosmological
distance to the source and $ z $ is the redshift of the source. 
When $ m_g \rightarrow 0 \Rightarrow \delta \Psi(f) \rightarrow 0 $, which is the case predicted by general relativity. 

\cref{fig:m_g_GW150914} shows the cumulative posterior probability distribution for $\lambda_g$ of the detection of GW150914. 
Note that the 90 \% confidence interval of $\lambda_g $ is $ \sim 10^{14} $ km, which is of the same order of the luminosity distance of GW150914. 
This order of constraint can be explained in the light of an estimation of order of magnitude. 
\cref{eq:dephasing} compares the Compton's wavelength of the phenomenological graviton to the luminosity distance. 
Therefore, it comes with no surprise that the constraint is of the same order of the propagation distance of gravitational waves from the source to detectors. 
\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{m_g_GW150914.png}
  \caption{The cumulative posterior probability distribution for $\lambda_g$ from the detection of GW150914, reproduced from \cite{LIGO_07}. Note that the 90 \% confidence interval of $\lambda_g \sim 10^{14} $ km, which is of the same order of the luminosity distance of GW150914. }
  \label{fig:m_g_GW150914}
\end{figure}
The results about the Compton's wavelength of the phenomenological graviton obtained from GW150914 corresponds to the mass of $ \sim 10^{-23} $ eV, which is a very tight results. 

\subsection{Measuring the Temperature of Astrophysical Black Holes}

Unfortunately, before the work described in this thesis, there is no way to generically measure the temperature of astrophysical black holes. 
The author of this thesis devised the first test and obtained the first direct constraint on temperature of astrophysical black holes. 
Readers are referred to \cref{chap:BHTemp} for the test which is capable to measure the temperature of astrophysical black holes. 

\section{Concluding Remarks}

Even though a lot of new tests of general relativity were made possible by direct detection of gravitational waves, these new tests still have some limitation. 

Firstly, these tests mostly rely on the far-field propagation of gravitational waves or the weak-field behaviour of black holes. 
For example, the test of gravitational-wave dispersion relies on only the far-field propagation of gravitational waves and is uninformative about the behaviour of compact objects due to the mass of graviton.
The post-Newtonian parameterized test is testing general relativity with the far-field inspiral behaviour of gravitational waves. 
These tests can still not fully testing general relativity in the strong-field regime. 

Secondly, the physical meaning of some of the existing tests is not transparent.  
Taking the post-Newtonian parameterized test and the IMR consistency test as examples, even if such tests really detect deviation from general relativity, the tests cannot point us to the missing physics. 
These limitations hinder us from thoroughly testing general relativity. 

In the coming chapters, this thesis will develop two new tests relying solely on the behaviour of the event horizon, namely the ringdown phase, and rich in physical meaning. 

