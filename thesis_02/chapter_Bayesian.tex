\chapter{Bayesian Parameter Estimation and Model Selection} \footnote{This chapter is largely based on the lectures by Prof. Chris Van Den Broeck. The lecture notes and video recording can be found at \url{https://www.icts.res.in/program/gws2015/talks}. } 
\label{chap:Bayesian}

\section{Introduction}

Detection of gravitational waves is extremely difficult as gravitational-wave signals are always amongst overwhelming noises. 
To extract the signal and estimate various parameters of source compact binary, we employ the techniques of Bayesian inference. 
Furthermore, Bayesian inference enables us to rank different hypothesis, for example, if general relativity is the most consistent theory with detected signals?
This kind of data analysis technique is essential, not only, to gravitational-wave astronomy, but also to other fields of fundamental physics, for example, particle physics \cite{LHC_01} and imaging black holes by the Event-horizon Telescope \cite{EHT_04}. 

This chapter is devoted to give a brief introduction of Bayesian inference. 
In particular, we will focus on parameter estimation and hypothesis ranking by Bayesian inference. 

\section{Parameter Estimation}
\label{sec:PE}

\subsection{Brief Introduction of Bayes Theorem}

The goal of parameter estimation is to construction the posterior of various parameters by analysing the signals, given \textit{what we have known}. 
Parameter estimation makes use of Bayes' theorem. 
Consider the probability that both $ A $ and $ B $ is true $ p(A, B) $, where $ A $ and $ B $ are two statements. 
Then, by the product rule, 
\begin{equation}
p(A, B) = p(A|B) p(B), 
\end{equation}
where $ p(B) $ is the probability that $ B $ is true and $ p(A|B) $ is the probability that $ A $ is true \textit{given} that $ B $ is true.
Since $ p(A, B) $ is symmetric over $ A $ and $ B $, by switching $ A $ and $ B $, we also have 
\begin{equation}
p(A, B) = p(B|A) p(A).  
\end{equation}
Therefore, 
\begin{equation}\label{eq:sim_Bayes_theorem}
p(A|B) = \frac{p(B|A) p(A)}{p(B)}. 
\end{equation}
\cref{eq:sim_Bayes_theorem} is the central statement of Bayes' theorem. 

We now start introducing Bayes' theorem into the content of gravitational-wave astronomy.
We let the parameters describing the properties of a compact binary be $ \vec{\theta}$ (see the following discussion for exact definitions) and the detected signals be $ d $. 
Then we also write the following, 
\begin{itemize}
\item \textit{prior}, the probability that a compact binary is of $ \vec{\theta}$, $ p(\vec{\theta}| H, I) $, also commonly denoted as $ \pi(\vec{\theta})$, 
\item \textit{likelihood}, the probability that a compact binary of $ \vec{\theta}$ will produce data $ d $, as $ p(d|\vec{\theta}, H, I) $, also commonly denoted as $\mathcal{L}(d|\vec{\theta}, H, I)$, 
\item \textit{evidence}, the probability that signal $ d $ is detected, $ p(d| H, I) $, also commonly denoted as $ Z $, and 
\item \textit{posterior}, which is always our goal, the probability that the detected signal is due to a compact binary of $ \vec{\theta}$, as $ p(\vec{\theta}| d, H, I) $. 
\end{itemize}
As a convention in gravitational-wave astrophysics, we always write down the symbols of $ H $ and $ I $ without explicitly stating their meanings. 
$ H $ mean the hypothesis that, for example, general relativity is correct and we have choosen reasonable priors etc. and $I$ means "all the background information we have". 
Note that evidence does not depend on $ \vec{\theta} $. 

In terms all these symbols and definitions, we convert \cref{eq:sim_Bayes_theorem} into 
\begin{equation}
\label{eq:Bayes_theorem_GW}
p(\vec{\theta}|d, H, I) = \frac{p(d|\vec{\theta}, H, I) p(\vec{\theta}| H, I)}{p(d|H, I)}. 
\end{equation}

\subsection{Definitions of Parameters in Gravitational-wave Astronomy}

Specifically, for compact binary coalescences, $ \vec{•}\theta} $ at least includes 
\begin{itemize}
\item the masses of the parental black holes $ m_1 $ and $ m_2 $, also commonly expressed as the chirp mass $ \mathcal{M} = \frac{m_1^{3/5} m_2^{3/5}}{(m_1+m_2)^{1/5}}$ and symmetric mass ratio $ \eta = \frac{m_1 m_2}{ (m_1 + m_2)^2} $,  
\item the spins of the parental black holes, $ \vec{\chi}_1 $ and $ \vec{\chi}_2 $,  
\item the luminosity distance $ d_L$ to the parental black holes, 
\item the inclination $ \iota $ between the orbital plane of the parental black holes to the line of sight, and 
\item the sky location of the parental black holes.  
\end{itemize}
In reality, more parameters will be included for detection logistic, for example, the arrival time differences between various detectors. 
Special considerations may also be taken if the detected signal involves neutron star(s). 
For neutron-star-involved merger, parameters like tidal deformability will also be included \cite{LIGO_06}. 
Note that all these considerations are a part of our background information $ I $. 

As discussed, for each of the inference parameters, we have to prescribe a prior probability distribution, which encodes our prior knowledge of the concerned parameters. 
Unless we have prior information, otherwise, the prior should be as unbiased as possible.
Usually uniform priors (also known as "flat" prior) are choosen so that within a particular range, every possibility is equally probable. 
For example, we usually choose uniform prior for $ \mathcal{M}_{\rm c} $ and $ \eta $, etc, because we have no solid prior information about the mass distribution of compact binaries.


\subsection{Definitions of Likelihood}

We assume that the noise in our detector is stationary Gaussian. 
With this assumption in place, we can \textit{assign} the likelihood as \cite{Cutler_Flanagan_1994}, 
\begin{equation}\label{eq:likelihood}
p(d|\vec{\theta}, H, I)  \propto \exp \left( - \frac{1}{2} \braket{d - h(\vec{\theta}) | d - h(\vec{\theta})} \right), 
\end{equation}
where $ h(\vec{\theta}) $ is the gravitational-wave template generated with parameters $ \vec{\theta} $ and the braket notation denotes the noise-weighted inner product. 
In the time domain, the noise-weighted inner product is defined as 
\begin{equation}
\braket{d - h(\vec{\theta}) | d - h(\vec{\theta})} = \iint d t d \tau & (d(t)-h(t|\vec{\theta})) \mathcal{C}^{-1}(\tau) (d(t+\tau)-h(t+\tau|\vec{\theta})),
\end{equation}
where $ \mathcal{C}(\tau) $ is the two-point autocovariance function of noise, defined by
\begin{equation}
\mathcal{C}(\tau) = \int dt n(t) n(t + \tau), 
\end{equation}
where $ n(t) $ denotes the noises. 
In the frequency domain, the noise-weighted inner product is defined as 
\begin{equation}
\braket{a | b} = 4 \Re \int_{0}^{+\infty} d f \frac{a(f)b^{\dagger}(f)}{S_h (f)}, 
\end{equation}
where $ S_h (f) $ is the power spectral density (P.S.D.) of the detector. 

\subsection{Extracting the Posteriors through Nested Sampling}

As mentioned, evidence does not depend on $ \vec{\theta} $ and is just a proportionality constant. 
Also, more often than not, priors are choosen to be uniform. 
If priors of all parameters are uniform, 
\begin{equation}
p(\vec{\theta}|d, H, I) \propto p(d|\vec{\theta}, H, I). 
\end{equation}
In other word, if every prior is uniform, extracting posterior is equivalent to extracting likelihood from data. 
However, even with this simplification, it is still extremely computational costly to extract the posterior (or likelihood) of 15 dimensions (as we have to extract the posteriors of at least 15 parameters when analysing a gravitational-wave event).  
Fortunately there is a sampling method, known as the nested sampling \cite{Nested_Sampling}, to efficiently estimate the posterior. 

In fact, nested sampling focuses on the computation of evidence and obtain the likelihood as a by product. 
To compute evidence, we need to compute a higher-dimensional integral, 
\begin{equation}
\begin{split}
p(d|H, I) &= \int d \vec{\theta} p(d|\vec{\theta}, H, I) p(\vec{\theta}| H, I) \\
&= \int d \vec{\theta} p(d|\vec{\theta}, H, I) \pi(\vec{\theta}). 
\end{split}
\end{equation}
The key ideas of nested sampling is sketched as follow,  
\begin{itemize}
\item We define a variable $ X $, known as the prior mass, which is the fraction of prior volume with likelihood greater than $ \lambda $ (a number), 
\begin{equation}
X = \int_{L(\vec{\theta})>\lambda} d \vec{\theta}\pi(\vec{\theta}) d \vec{\theta}. 
\end{equation}
Hence, 
\begin{equation}
d X = \pi(\vec{\theta}) d \vec{\theta}. 
\end{equation}
This change of variable reduces the computation of evidence into an one-dimensional integration, 
\begin{equation}
p(d|H, I) = \int d X L(X). 
\end{equation}
\item When performing parameter estimation, we search for a set of points in the parameter space, say $ \vec{\theta}_{i} $ in the $i$th step, such that $ L (\vec{\theta}_{i}) > L (\vec{\theta}_{i-1}) $ and allow us to \textit{assign} $ X_{i} < X_{i-1} $. 
\item Then the evidence can be approximated as 
\begin{equation}
Z = \int d X L(X) \approx \sum_i L_i \Delta X_i \approx \sum_i L_i (X_i - X_{i-1}). 
\end{equation}
\end{itemize}

In other words, suppose that we have placed $ N $ samples in the parameter space $ \theta_{i} $ where $ i = 1, 2, ..., N $, and therefore we also have a sequence of likelihoods $ L_i $. 
Then, initially, we assign each sample a prior mass \textit{element} as 
\begin{equation}
\Delta X = \frac{1}{N \text{(number of samples)}}. 
\end{equation}
We then \textbf{sort} the likelihood sequence in accenting order.
Of course, the likelihood could then be evaluated as 
\begin{equation}
Z = \sum_i L_i \Delta X_i = \frac{1}{N} \sum_i L_i. 
\end{equation}
Then the likelihood for $ X = \frac{1}{M} $ can then be approximated by the values of the $ \sim N / M $th element in the likelihood sequence.
Finally, by binning the obtained parameter sequence, we obtain the desired posterior as a function of $ \vec{\theta} $. 

The above description just give a sketch of the nested sampling. 
For more detail discussion and the exact algorithm, the readers are referred to a plenty of available references, see \cite{Tjonnie_thesis} for example. 
In reality, there are a number of existing pipelines in the community of gravitational-wave astrophysics for extracting the posterior by feeding in detected signal through nested sampling or Markov-chain Monte Carlo algorithms. 
The standard pipeline used by the Advanced LIGO and Virgo is the LIGO Algorithms Library Suite (more commonly known as the \texttt{LALSuite}). 
For the detail of about the pipeline, please refer to \cite{LALInference}. 
For nested sampling for general purposes other than parameter estimation in gravitational-wave astronomy, packages, such as \texttt{PyMultiNest} \cite{PyMultiNest} and \texttt{CPNest}\cite{CPNest}, are also available. 

\subsection{Marginalization of Posteriors}

In some occasions, we shall focus more on some particular parameters rather than every parameters. 
Especially when we test general relativity, we will like focus more on the testing-general-relativity parameters. 
In such cases, we can obtain the \textit{marginalized} posteriors of particular parameter(s), say $ \theta_1 $
\begin{equation}
p(\theta_1|d, H, I) = \int_{\theta_{2}^{\rm min}}^{\theta_{2}^{\rm max}} ... \int_{\theta_{N}^{\rm min}}^{\theta_{N}^{\rm max}} p(\vec{\theta}|d, H, I) d \theta_2 ... d \theta_N. 
\end{equation}

\section{Model Selection}

Very often in analysing the data, we need to pin-point the hypothesis which is the most consistent with detection. 
For example, from the prospective of testing general relativity, we need to test if general relativity remains the best gravity theory against other alternative theories. 
For this purpose we need to perform model selection. 

\subsection{Odd Ratio and Bayes Factor}

Going back to the very beginning of our discussion, we recall that we cannot perform Bayesian inference without assuming any prior knowledge or hypothesis.
Also, evidence is actually the probability that we have detected the signal given the hypothesis $ H $. 
If we assume different hypothesis $ H' $, the evidence may (very likely) be different, 
\begin{equation}
p(d|H', I) = \int d \vec{\theta}' p(d|\vec{\theta}', H', I) p(\vec{\theta}'| H, I). 
\end{equation}
Note also that, the number of parameters in $ H' $ (i.e. $ \dim \theta' $) may be different from that of $ H $. 
In view of this difference, we can define the \textit{odd ratio} of $ H $ against $ H' $, 
\begin{equation}
\mathcal{O}_{H'}^{H} = \frac{p(H|d, I)}{p(H'|d, I)} = \frac{p(d|H, I)p(H|I)}{p(d|H', I)p(H'|I)} = \frac{p(d|H, I)}{p(d|H', I)} \frac{p(H|I)}{p(H'|I)}. 
\end{equation}
If $ H $ is favoured in detection, $ \mathcal{O}_{H}^{H'} > 1$ and $ \mathcal{O}_{H}^{H'} < 1 $ otherwise.
In particular, 
\begin{equation}
\frac{p(H|I)}{p(H'|I)}
\end{equation}
is called the prior odd and 
\begin{equation}
\mathcal{B}_{H'}^{H} = \frac{p(d|H, I)}{p(d|H', I)}, 
\end{equation}
is known as the Bayes factor. 
The best hypothesis is the hypothesis of highest evidence. 

\subsection{Occam's razor}

However, another question emerges: one may wonder if an incorrect hypothesis with more free parameters will have higher evidence as it can always fit the data in a better way.  
% Firstly, as hinted, different hypothesis can take different number of free parameters. 
% Secondly, a hypothesis with more free parameters will increase the likelihood. 
% An incorrect hypothesis with sufficient number of free parameter can have a higher evidence than the correct one w=ith less parameters. 
Fortunately, when performing Bayesian model selection, an model or hypothesis with higher number of free parameters will be penalized. 
We can thus avoid pin-pointing an incorrect hypothesis which seems to be correct just because it can better fit the data with redundant free parameters. 

As an example, we explore two hypothesis, $ X $ and $ Y $, where $ X $ is the correct hypothesis which takes no free parameter and $ Y $ is the incorrect one, taking one free parameter, say $ \lambda $. 
Suppose the prior of $ \lambda$ is unbiased and we only consider a range of$ \lambda \in [\lambda_{\rm min}, \lambda_{\rm max} ]$, then 
\begin{equation}
p(\lambda | Y, I)=\frac{1}{\lambda_{\max }-\lambda_{\min }}.
\end{equation}
Moreover, to a good approximation, we approximate $ p(d|\lambda, Y, I) $ as a Gaussian distribution, 
\begin{equation}
p(d | \lambda, Y, I) \approx p\left(d | \lambda_{0}, Y, I\right) \exp \left(-\frac{\left(\lambda-\lambda_{0}\right)^{2}}{2 \sigma_{\lambda}^{2}}\right) = \frac{p\left(d | \lambda_{0}, Y, I\right)}{\lambda_{\max }-\lambda_{\min }} \exp \left(-\frac{\left(\lambda-\lambda_{0}\right)^{2}}{2 \sigma_{\lambda}^{2}}\right), 
\end{equation}
where $ \lambda_0 $ is the value of $ \lambda $ which maximizes the likelihood and $ \sigma_{\lambda} $ is the "width" of the distribution. 
As we have choosen a uniform prior for $ \lambda $, $ \lambda_0 $ actually corresponds to the value which the data is best fitted with $ \lambda $ and $ \sigma_{\lambda} $ is the experimental uncertainty. 
To compute the evidence of $ Y $, we need to marginalize the posterior of $ Y $ over $ \lambda $, 
\begin{equation}
p(d|Y, I) = \int d \lambda p(d|\lambda, Y, I) p(\lambda|Y, I) = p\left(d | \lambda_{0}, Y, I\right) \frac{\sigma_{\lambda} \sqrt{2 \pi}}{\lambda_{\max }-\lambda_{\min }}. 
\end{equation}
Therefore, the odd ratio of $ X $ against $ Y $ will be
\begin{equation}
O_{Y}^{X}=\frac{p(X | I)}{p(Y | I)} \frac{p(d | X, I)}{p\left(d | \lambda_{0}, Y, I\right)} \frac{\lambda_{\max }-\lambda_{\min }}{\sigma_{\lambda} \sqrt{2 \pi}}. 
\end{equation}
Note that $ \frac{p(X | I)}{p(Y | I)} \frac{p(d | X, I)}{p\left(d | \lambda_{0}, Y, I\right)} $ compares $ X $ with the best fitted $ Y $. 
However, $ O_{Y}^{X} $ carries an extra factor of $ \frac{\lambda_{\max }-\lambda_{\min }}{\sigma_{\lambda} \sqrt{2 \pi}} $. 
In particular if $ \lambda $ is redundant, it will usually have high experimental uncertainty, which means that $ \frac{\lambda_{\max }-\lambda_{\min }}{\sigma_{\lambda} \sqrt{2 \pi}} < 1 $. 
Thus, the factor of $ \frac{\lambda_{\max }-\lambda_{\min }}{\sigma_{\lambda} \sqrt{2 \pi}} $, due to an unnecessary parameter, penalizes the corresponding hypothesis ($Y$). 

From the above example, we conclude that for the purposes of parameter estimation or model selection, we should only use the minimum number of necessary parameters, which is the manifestation of Occam's razor in gravitational-wave astronomy. 

\section{Concluding Remarks}

\iffalse

\section{The Art of Testing General Relativity Through Gravitational-wave Detection}

Detection of gravitational waves provides us a new novel way to test general relativity. 
The behaviour and the resultant gravitational waves due to compact binary coalescences can be uniquely determined by general relativity. 
Thus, any deviation from prediction indicates the break down of general relativity. 
If we can parameterize deviation of gravitational waves detected from the prediction, we can estimate the posterior of the deviation through the procedure outlined in \cref{sec:PE} and thereby be a test of general relativity. 

\begin{figure}[htp!]
  \centering
  \includegraphics[width=\columnwidth]{TGR_Flowchart.pdf}
  \caption{The flow chart which sketches the pathway when designing a new test of general relativity through gravitational-wave detection.}
  \label{fig:flow_chart}
\end{figure}
\cref{fig:flow_chart} sketches the pathway of designing a new test of general relativity include the following pathway, 
\begin{enumerate}
\item Consider a motivated correction of general relativity. 
For the sake of your survival in academia, you are advised to consider a really motivated correction of general relativity. 
Then derive the modification to gravitational waveforms due to the correction considered. 
For example, this thesis focuses on the correction due to Lorentz violation and quantum gravity, which are popular alternative gravity theories. 
\item Work out the effects of the considered corrections on gravitational-wave generation.
In particular, this thesis focuses on the generation of gravitational waves during the ringdown phase. 
Unless with reasonable justification \footnote{For example, the vDVZ discontinuity in massive gravity theories \cite{Massive_Gravity_01, Massive_Gravity_02}, which is a discontinuity fail to reduce to general relativity when consider the limit of zero graviton mass}, as a rule of thumb, when alternative physics is switched off, the waveforms should reduce to the known prediction by general relativity. 
\item Parameterize the modification. 
We must first check that the parametrization must not be degenerate with the effects due to other existing parameters, for example, masses and spins. 
Otherwise, we must look for an alternative parametrization which can free us from the degeneracy, or the whole test could have been scrapped as we cannot really tell if the changes of waveforms are due to alternative physics or other intrinsic properties of the source.
For example, when exploring the signature of black-hole radiation in ringdown, if we parameterize the radiation signature by the amplitude of the radiation, then the posterior will be degenerate with the luminosity distance.
To escape from this degeneracy, we parameterize the temperature instead.   
Degeneracy can be checked by studying similarity between waveforms in general relativity and those with parameterized modification. 
\item Implement the modification and register the violation parameters into existing pipelines for posterior estimation. 
There are some tips concerning the registration of the parameters. 
If the parameter is one-sided, for example, mass of some particles or temperature, which only permitted for positive number and values of the concerned parameter should be zero, as predicted by general relativity, then it will be more convenient if the parameter is inferred in decade form, i.e. Inferring the $ \log_{10} $ of the values instead of the values. 
\end{enumerate}

\fi 



