\contentsline {chapter}{Abstract}{i}{Doc-Start}
\contentsline {chapter}{Acknowledgement}{ii}{chapter*.1}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Introduction}{4}{chapter.2}
\contentsline {chapter}{\numberline {3}Black-Hole Perturbation with Spinor Coefficients}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Background Study}{10}{chapter.4}
\contentsline {chapter}{\numberline {5}Conclusion}{11}{chapter.5}
\contentsline {chapter}{\numberline {A}Equation Derivation}{12}{appendix.A}
\contentsline {chapter}{Bibliography}{13}{appendix.A}
