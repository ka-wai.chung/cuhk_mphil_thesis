% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.



\documentclass{beamer}

%
% DO NOT USE THIS FILE AS A TEMPLATE FOR YOUR OWN TALKS�!!
%
% Use a file in the directory solutions instead.
% They are much better suited.
%


% Setup appearance:

\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}


% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{braket}
\usepackage{subfigure}
\usepackage{graphicx}
\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}

% Setup TikZ

\usepackage{tikz}
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usefonttheme{professionalfonts}
\usepackage[backend=biber]{biblatex}
\bibliography{bibliography.bib}

% Removes icon in bibliography
\setbeamertemplate{bibliography item}{}

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
  Testing General Relativity through Black-Hole Ringdown%
}

\author[Chung]
{
  Adrian K.W. Chung\inst{1}
}

\institute[CUHK]
{
  \inst{1}%
  Department of Physics, 
  The Chinese University of Hong Kong
  , Shatin, N.T.
  , Hong Kong
}

\date[TGR Call]
{"Invited" GR lecture , 18${}^{\rm th}$ April 2019}

% Setting page number
\setbeamertemplate{sidebar right}{}
\setbeamertemplate{footline}{%
\hfill\usebeamertemplate***{navigation symbols}
\hspace{1cm}\insertframenumber{}/\inserttotalframenumber}

% The main document

\begin{document}

\begin{frame}
  \titlepage
  \begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{logo-cuhk.png}
\end{figure}
\end{frame}

\begin{frame}{The Scope of My Works}
\begin{figure}[h]
  \includegraphics[scale=0.4]{Group_Photo_01}
\end{figure}
\end{frame}

\begin{frame}
\begin{enumerate}
\item Traditional Method of Black-Hole Perturbation
\item Black-Hole Perturbation in the Newman-Penrose Formalism
\item Constraining $m_g$ through Black-Hole Ringdown
\item Constraining Black-Hole Temperature through Ringdown Detection
\end{enumerate}
\end{frame}

\section{BH Perturbation}

\begin{frame}{Black Hole Perturbation}
\begin{itemize}
\item Perturbed field equations: $ g_{\mu \nu} \rightarrow g_{\mu \nu} + h_{\mu \nu}$
\begin{equation}
\delta G_{\mu \nu} [h_{\mu \nu}] = \delta T_{\mu \nu}
\end{equation}
\item Regge-Wheeler Equation, Zerilli Equation ...... 
\item Applicable in alternative theories
\item May not be separable
\end{itemize}
\end{frame}

\begin{frame}{Metric Perturbation}
\begin{itemize}
\item Consider $ g_{\mu \nu} = g_{\mu \nu}^{(0)} + h_{\mu \nu} $, then 
\begin{equation}
\begin{split}
\Gamma^{\mu}_{\nu \lambda} &= \Gamma^{\mu (0)}_{\nu \lambda} + \delta \Gamma^{\mu}_{\nu \lambda}, \\
\delta \Gamma^{\mu}_{\nu \lambda} &= \frac{1}{2} g^{\mu \rho}_{(0)} \big( \partial_{\nu} h_{\rho \lambda} + \partial_{\lambda} h_{\rho \nu} - \partial_{\rho} h_{\nu \lambda} \big), \\
R_{\mu \nu} &= R_{\mu \nu}^{(0)} + \delta R_{\mu \nu} \\
\delta R_{\mu \nu} &= \nabla_{\nu} \delta \Gamma^{\rho}_{\mu \rho} - \nabla_{\rho} \delta \Gamma^{\rho}_{\mu \nu}. 
\end{split}
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Metric Perturbation: Axial Part (Odd Parity)}
\begin{itemize}
\item Within the Regge-Wheeler gauge, the perturbations are described by two axial ($h_0, h_1$) and four polar functions ($H_0, H_1, H_2, K $), 
\begin{equation}\label{eq:perturbation}
h_{\mu \nu} = h^{\rm polar}_{\mu \nu} + h^{\rm axial}_{\mu \nu}, 
\end{equation}
where 
\begin{equation}\label{eq:perturbation_axial}
h^{\rm axial}_{\mu \nu} = 
\begin{pmatrix} 
0 & 0 & \frac{h_0}{\sin \theta} \partial_{\phi} Y_{lm}  & - h_0 \partial_{\theta} Y_{lm} \\ 
0 & 0 & \frac{h_1}{\sin \theta} \partial_{\phi} Y_{lm}  & - h_1 \partial_{\theta} Y_{lm} \\ 
\frac{h_0}{\sin \theta} \partial_{\phi} Y_{lm} & \frac{h_1}{\sin \theta} \partial_{\phi} Y_{lm} & 0 & 0\\
- h_0 \partial_{\theta} Y_{lm} & - h_1 \partial_{\theta} Y_{lm} & 0 & 0 \\ 
\end{pmatrix}, 
\end{equation} 
\end{itemize}
\end{frame}

\begin{frame}{Metric Perturbation: Polar Part (Even Parity)}
\begin{equation}\label{eq:perturbation_polar}
h^{\rm polar}_{\mu \nu} = 
\begin{pmatrix} 
- A(r) H_0 Y_{lm} & - H_1 Y_{lm} & 0 & 0 \\ 
 -H_1 Y_{lm} & - \frac{1}{A(r)} H_2 Y_{lm} & 0 X & 0 \\
0 & 0 & -r^2 K Y_{lm} & 0\\
0 & 0 & 0 & - r^2 \sin^2 \theta K Y_{lm} \\ 
\end{pmatrix}.
\end{equation} 
where $ A(r) = 1 - 2M / r $. 
\end{frame}

\begin{frame}{The Regge-Wheeler Equation \footnote{Regge T and Wheeler J A 1957 Phys. Rev. 108 1063} Axial Part (Even Parity)}
After some algebra (transformations, redefinition of functions), we arrived at 
\begin{equation}
\frac{\partial^2 Z^{(-)}}{\partial x^2} + \Big( \omega^2 - V(r)\Big) Z^{(-)} = 0, 
\end{equation}
where $ x $ is the tortoise coordinate, $ Z^{(-)} $ is a function of $ h_0 $ and $ h_1 $ and 
\begin{equation}\label{eq:V_RW}
V(r) = \Big( 1 - \frac{2M}{r} \Big) \Big(\frac{l(l+1)}{r^2} - \frac{6M}{r^3} \Big). 
\end{equation}
\end{frame}

\begin{frame}{The Zerilli Equation \footnote{Frank J. Zerilli
Phys. Rev. Lett. 24, 737} Polar Part (Odd Parity)}
\begin{itemize}
\item To obtain the master equation for the polar sector of metric perturbation, the algebra is even more dirty. 
\item Anyway, the equation is 
\begin{equation}
\frac{\partial^2 Z^{(+)}}{\partial x^2} + \Big( \omega^2 - V(r)\Big) Z^{(+)} = 0, 
\end{equation}
where 
\begin{equation}
V(r) = \Big( 1 - \frac{2M}{r} \Big) \frac{2 \lambda^2 (\lambda + 1) r^3 + 6 \lambda^2 M r^2 + 18 \lambda M^2 r + 18 M^3}{r^3 (\lambda r + 3M)^2}. 
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Effective Potentials}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{Effective_Potential}
\end{figure}
Taken from M. Maggiore \textit{Gravitational Waves Vol.2}, Chapter 12. The horizontal axis stands for $ x $. The vertical axis stands for $ V $. Solid lines plot the $ V $ for the RG equation; Dash lines plot the $ V $ for the  Zerilli equation.  
\end{frame}

\begin{frame}{Introduction to Quasinormal Modes}
\begin{itemize}
\item The Regge-Wheeler equation and the Zerilli equation look like the Sch\"{o}dinger equation. 
\item Nevertheless, they don't give normal modes. 
\item Instead, their eigenvalues are complex. Why? 
\end{itemize}
\end{frame}

\begin{frame}{Boundary Conditions}
\begin{itemize}
\item At the event horizon, everything is purely ingoing (\color{red}{really?!}\color{black}).
\begin{equation}
Z(x \rightarrow - \infty) \propto e^{ - i \omega x}. 
\end{equation} 
$\Rightarrow $ Energy does not conserved. \\
$\Rightarrow $ There will be no normal modes. \\ 
$\Rightarrow $ So, quasinormal (decaying) modes. \\
\item At spatial infinity, to be detectable,  
\begin{equation}
Z(r \rightarrow + \infty) \propto e^{ i \omega r}. 
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Pros and Cons of the Traditional Methods}
\begin{itemize}
\item Applicable in alternative theories
\item May not be separable
\item Unable to handle rotating black holes .....
\item Might not be separable in alternative theories. 
\end{itemize}
\end{frame}

\section{NP Formalism}

\begin{frame}{Introduction to the Newman-Penrose Formalism}
\begin{itemize}
\item I am lazy. Go check the wiki \footnote{\url{https://en.wikipedia.org/wiki/Newman-Penrose_formalism}} .... 
\end{itemize}
\end{frame}

\begin{frame}{The Teukolsky Equation \footnote{Saul A. Teukolsky Phys. Rev. Lett. 29, 1114. See also Astrophysical Journal, Vol. 185, pp. 635-648 (1973) for detail account}}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{TE}
\end{figure}
\end{frame}

\begin{frame}{The Teukolsky Equation}
\begin{equation}
\begin{split}
&\Bigg( \frac{(r^2 + a^2)^2}{\Delta} - a^2 \sin^2 \theta \Bigg) \frac{\partial^2 \psi}{\partial t^2} + \frac{4 Mar}{\Delta} \frac{\partial^2 \psi}{\partial t \partial \phi} + \Bigg( \frac{a^2}{\Delta} - \frac{1}{\sin^2 \theta} \Bigg) \frac{\partial^2 \psi}{\partial \phi^2} \\
&- \Delta^{-s} \frac{\partial }{\partial r} \Big( \Delta^{s+1} \frac{\partial \psi}{\partial r}\Big) - \frac{1}{\sin \theta} \frac{\partial }{\partial \theta} \Big( \sin \theta \frac{\partial \psi}{\partial \theta} \Big) \\
& - 2s\Bigg( \frac{a(r-M)}{\Delta} + i \frac{\cos \theta}{\sin^2 \theta} \Bigg)\frac{\partial \psi}{\partial \phi}  - 2s \Bigg(\frac{M(r^2 - a^2)}{\Delta} - r - ia \cos \theta \Bigg) \frac{\partial \psi}{\partial t} \\
&+(s^2 \cot^2 \theta - s) \psi = 0.
\end{split}
\end{equation}
\end{frame}

\begin{frame}{Separation of Variables}
\begin{itemize}
\item The Teukolsky equation is separable (miracle!). 
\item Let $ \psi = R(r) S(\theta) e^{i m \phi}e^{ -i \omega t}$ and $ u(r, t) = \sqrt{r^2+a^2} \Delta^{-1} R(r, t) $ , then $ u $ satisfies 
\begin{equation}\label{eq:SE}
\frac{\partial^2 u}{\partial x^2} + \Big( \omega^2 - V(r) \Big) u = 0, 
\end{equation}
where $ V $ is a complex potential. 
\end{itemize}
\end{frame}


\section{Constraining $m_g$}

\begin{frame}
\Huge Constraining $m_g$ through Black-Hole Ringdown 
\end{frame}

\begin{frame}{Existing Constraints on the Graviton Mass}
\begin{itemize}
\item Dispersion with non-zero mass of graviton 
\begin{equation}
\omega^2 - k^2 = m_g^2 
\end{equation}
$\Rightarrow$ different speed for different $ \omega $ \\
$\Rightarrow $ Phase lag 
\item Quasinormal mode frequencies should be changed by mass of graviton ($m_g$) \\
$ \Rightarrow $ propagation velocity of gravitational waves \\
$ \Rightarrow m_g$ contributes to the effective potential \\
  \end{itemize}
\end{frame}

\begin{frame}{The Central Problem }
\Huge How do the QNM frequencies change if $ m_g \neq 0 $ ? 
\end{frame}

\begin{frame}{The Teukolsky Equation}
\begin{itemize}
\item When scalar perturbation is considered, 
\begin{equation}
\text{T.E.} \Rightarrow \Sigma g^{\mu \nu} \nabla_{\mu} \nabla_{\nu} \psi = 0, 
\end{equation}
where $ \Sigma = r^2 + a^2 \cos^2 \theta $.
\item In far field, $ r \rightarrow + \infty $
\begin{equation}
\text{T.E.} \rightarrow \frac{\partial^2 \psi}{\partial r^2} - \frac{\partial^2 \psi}{\partial t^2} = 0.
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{The Modified Teukolsky equation}
\begin{itemize}
\item For massive field, we demand, $ r \rightarrow + \infty $
\begin{equation}
\text{T.E.} \rightarrow \frac{\partial^2 \psi}{\partial r^2} - \frac{\partial^2 \psi}{\partial t^2} +m^2 \psi = 0 .
\end{equation}
\item Vacuum perturbation of field mass $ m $, we modified the T.E. as
\begin{equation}
\mathcal{L} \psi + m^2 \Sigma \psi = 0.
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Effects on Quasinormal Modes}
  \begin{itemize}
  \item As $ m_g \approx 0 \Rightarrow m_g^2 << \omega $, perturbation leads to 
  \begin{equation}
  \tilde{\omega}_{nlm} \approx \tilde{\omega}^{(0)}_{nlm} + \frac{m_g^2}{2 \tilde{\omega}^{(0)}_{nlm}}.
  \end{equation}
  \item The frequencies of the $ nlm $ overtone 
  \begin{equation}
  \omega_{nlm} = \text{Re} \tilde{\omega}_{nlm} = \omega^{\rm Re}_{nlm}.
  \end{equation}
  \item The lifetimes of the  $ nlm $ overtone 
  \begin{equation}
  \tau_{nlm} = 1 / \text{Im} \tilde{\omega}_{nlm}  = 1 / \omega^{\rm Im}_{nlm}.
  \end{equation} 
  \end{itemize}
\end{frame}

\begin{frame}{Derivation of the Frequency Shift}
\begin{itemize}
\item By separation of variables $ \psi = R(r) S(\theta, \phi) e^{-i \omega t} $, and let $ u = \Delta^{s/2} (r^2 + a^2)^{1/2} R $, where $ \Delta = (r-r_+)(r-r_-) $, $ u $ satisfies, 
\begin{equation}
\frac{\partial^2 u}{\partial r_*^2} + \Bigg( \omega^2 - V(r) - \color{red} m_g^2 \frac{r^2 \Delta}{(r^2 + a^2)^2}\color{black} \Bigg) u = 0,
\end{equation}
\item Denote $ V^{(1)} = m_g^2 \frac{r^2 \Delta}{(r^2 + a^2)^2}\color{black} $, by logarithmic perturbation theory \footnote{P. T. Leung, Y. T. Liu, W.-M. Suen, C. Y. Tam, K. Young, Phys.Rev. D59 (1999) 044034. Also }. 
\begin{equation}
\omega^{(1)} = \frac{\braket{u|V^{(1)}|u}}{2 \omega^{(0)} \braket{u|u}} .
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Changes of Quasinormal-mode Frequencies}
\begin{itemize}
\item Both $ \omega_{nlm} $ and $ \tau_{nlm} $ are increased by the graviton mass\footnote{Our results are consistent with the conclusion in R Brito, V Cardoso, and P Pani Phys. (2013) Rev. D 88, 023514, also arXiv:physics/9712037 [math-ph]}. 
\end{itemize}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.45]{QNM_022_f_M_plane_gradient_hor}
\end{figure}
\end{frame}

\begin{frame}{Ringdown Waveform \footnote{L London, D Shoemaker, and J Healy, (2014) Phys. Rev. D 90, 124032 } \footnote{
Carullo, van der Schaaf, London et al. (2018), arxiv:1805.04760}}
\begin{itemize} 
  \item Major overtones $ nlm = $ 022, 122, 033, 133, 044, 055, 021, 032 and 034 are included. 
  \begin{equation}
  h_{+} - i h_{\times} = \sum_{nlm} A_{nlm}(\eta, \chi) S_{lm} (\theta, \phi) e^{i \tilde{\omega}_{nlm} t}
  \end{equation}
\end{itemize}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.3]{TD_M_100}
\end{figure}
\end{frame}

\begin{frame}{Parameter Estimation}
\begin{itemize}
\item Ringdown signals, $ m_g = 0 $, by BH of $ 10 - 300 M_{\odot} $ at 400 Mpc.
\item Constraints of $ m_g \sim 10^{-12}-10^{-15} \rm eV $
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{Summary_plot_MMRDNS}
\end{figure}
\end{itemize}
\end{frame}

\begin{frame}{Summary}
\begin{itemize}
\item Phenomenological modification to the Teukolsky equation
\item Analytical (approximation) expression of QNM frequencies as a function of $ m_g $
\item Constraint of $ 10^{-15} $ eV of $ m_g $ due to intermediate mass black holes at 400 Mpc  
\end{itemize}
\end{frame}

\section{Temperature Measurement}

\begin{frame}
Constraining Black-Hole \color{red} Effective \color{black} Temperature through Ringdown Detection. 
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{Echoes_Memes}
\end{figure}
\end{frame}

\begin{frame}{Classical Boundary Condition}
\begin{itemize}
\item Denote $ \frac{1}{2} (\ddot{h}_{+} - i \ddot{h}_{\times}) = \psi_4 $, $ \rho^{-1} = r - i a \cos \theta $, $ \psi = \rho^{-4} \psi_4 $ satisfies the Teukolsky equation. 
\item Let $  \psi = R(r, t)S(\theta) e^{i m \phi} $, 
\begin{equation}
R(x \rightarrow - \infty, t) \propto \Delta^2 e^{- i \omega x }
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}{Boundary Condition for Quantum Black Holes}
\begin{itemize}
\item Boundary condition 
\begin{equation}
\begin{split}
R(x \rightarrow - \infty, t) \propto & S(\omega) \omega^2 \Delta^2 e^{- i \omega x } \\
& + \mathcal{H}(\omega) (\omega^{\dagger})^2 \Delta^2 e^{+i \omega^{\dagger} x + i \Phi(\omega)}
\end{split}
\end{equation}
\item For graviton emission by quantum black holes \footcite{D. Page, Phys. Rev. D 13, 198 1976}, 
\begin{equation}\label{eq:emittivity}
\mathcal{H}(\omega) \approx \frac{16 M^4}{45} \frac{A}{\pi} \frac{\omega^5}{e^{\beta \omega} - 1}, 
\end{equation}
where $ \beta = T_H^{-1} $. 
\end{itemize}
\end{frame}

\begin{frame}{Penrose Diagram for Quantum Black Holes}
\begin{figure}[htp!]
  \centering
  \includegraphics[scale=0.35]{Penrose_Diagram.pdf}
\end{figure}
\end{frame}

\begin{frame}{Ringdown Waveform of Black Holes with Horizon Effects}
\begin{itemize}
\item By the method of Green's function, 
\begin{equation}\label{eq:QBH_RD_Waveform}
\begin{split}
h_{+} - i h_{\times} &= \sum_{nlm} \Bigg( \underbrace{\frac{M}{r} A_{nlm}}_{1} + \underbrace{\frac{r_+}{r} C(\tilde{\omega}_{nlm})}_{2} \Bigg) e^{- i \tilde{\omega}_{nlm} t} \\ 
&+ \underbrace{\frac{16 M^4}{45} \frac{A}{\pi} \frac{r_+}{r} \int_{\omega_{\rm low}}^{\omega_{\rm high}} d \omega \frac{\omega^5 e^{-i \omega t}}{e^{\beta \omega} - 1}}_{3}. 
\end{split}
\end{equation}
\begin{enumerate}
\item Usual ringdown response. 
\item Additional excitation of quasinormal modes. 
\item Continuous emission of gravitational waves due to graviton emission. 
\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Ringdown Waveform of Black Holes with Horizon Effects}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{TD_waveform}
\end{figure}
\end{frame}

\begin{frame}{Bayesian Inference on $ \log T_H $}
\begin{itemize}
\item Bayesian inference on mock signals due to classical black holes of $ T_H = 0 $ of $ 80 - 200 M_{\odot} $ at 400Mpc. 
\item Essentially to estimate 
\begin{equation}
\begin{split}
& p(\log T_H|d, I) \\
& = \text{ posterior of $\log T_H$ given the detection} \\
& \text{ $ d $ assuming that the final black hole} \\
& \text{  radiates with spectrum of $ \mathcal{H}(\omega) = \frac{16 M^4}{45} \frac{A}{\pi} \frac{\omega^5}{e^{\beta \omega} - 1} $}. 
\end{split}
\end{equation} 
\end{itemize}
\end{frame}

\begin{frame}{Constraints}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.58]{Summary_plot}
\end{figure}
\end{frame}

\begin{frame}{Cross Correlations Between Ringdown Parameters}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{corner_plot_M_20_injected}
\end{figure}
\end{frame}

\begin{frame}{Summary}
\begin{itemize}
\item We propose a possible boundary condition of gravitational perturbation at the event horizon corresponding to quantum black holes
\item We study the ringdown phase of quantum black holes by solving the Teukolsky equation subjected to the B.C. 
\item From the (mock) signals due to classical black holes of $ 80 - 200 M_{\odot} $ at 400 Mpc, we demonstrate the ability to constraint the effective temperature up to $ \sim 10^{-8} $K. 
\end{itemize}
\end{frame}

\begin{frame}{GW in CUHK}
\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{Group_Photo_03}
\end{figure}
\end{frame}

\begin{frame}{Q and A}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{Is_it_linear}
\end{figure}
\end{frame}

\end{document}
